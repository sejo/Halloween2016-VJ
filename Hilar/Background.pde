class Background {
  Movie movie;
  String[] files;
  int ptr; 
  int size;
  
  boolean enabled;
  
  PApplet ap;
  Background(PApplet a){
    ap = a;
    ptr = 0;
    size = 10;
    files = new String[size];
    files[0] = "0.ruta de muertos_fondos_fotos intro.mp4";
    files[1] = "1. ruta de muertos_fondos_llorona.mp4";
    files[2] = "1.5 ruta de muertos_textos_TIRICIA_vf.mp4";
    files[3] = "2. ruta de muertos_fondos_cielo rojo.mp4";
    files[4] = "2.5ruta de muertos_textos_MUXE_vf.mp4";
    files[5] = "3. ruta de muertos_fondos_hasta la raiz.mp4";
    files[6] = "3.5 ruta de muertos_textos_CALAVERITA_vf.mp4";
    files[7] = "4. ruta de muertos_fondos_calaverita.mp4";
    files[8] = "5.ruta de muertos_fondos_faro.mp4";
    files[9] = "6.ruta de muertos_fondos_barricada.mp4";
    
   movie = new Movie(a,files[0]);
   movie.loop();
   
   enabled = false;
  }
  
  void draw(PGraphics p){
  //  p.resetMatrix();
  if(enabled){
    if(movie.available()==true){
      println("yes");
        movie.read();
    }
    p.image(movie,0,0,p.width,p.height);
  }
  else{
   p.fill(0);
   p.noStroke();
   p.rect(0,0,p.width,p.height); 
  }
   // image(movie,0,0,p.width,p.height);
  }
  
  int nextMovie(){
    ptr = (ptr+1)%size;
    movie = new Movie(ap,files[ptr]);
    movie.loop();
    return ptr;
  }
  
    int prevMovie(){
    ptr = (size+ptr-1)%size;
    movie = new Movie(ap,files[ptr]);
    movie.loop();
    return ptr;
  }
  
  boolean toggleEnabled(){
   enabled = !enabled;
   return enabled;
  }
  
}