class Bone {
  PImage img;
  float proportion; // w/h
  PVector po, pf, dif;
  ArrayList<PVector> ps;

  int mode; // 0 is two points, 1 is four points



  Bone(String path) {
    img = loadImage(path); 
    proportion = 1.0*img.width/img.height;
    po = new PVector();
    pf = new PVector();
    dif = new PVector();

    ps = new ArrayList<PVector>();

    mode = 0;
  }

  void draw(PGraphics pg) {

    float newheight = dif.mag();
    float newwidth = newheight*proportion;
    //println("New height: " + newheight+" Proportion: "+proportion+" New width: "+newwidth+" Prev width and height"+img.width+" "+img.height);
    pg.pushMatrix();
    if (mode==0) {//two points)
      pg.translate(po.x, po.y, po.z);
      pg.rotate(dif.heading()-PI/2);
      pg.beginShape(QUADS);
      pg.texture(img);
      pg.vertex(-newwidth/2, 0, 0, 0, 0);
      pg.vertex(newwidth/2, 0, 0, img.width, 0);
      pg.vertex(newwidth/2, newheight, 0, img.width, img.height);
      pg.vertex(-newwidth/2, newheight, 0, 0, img.height);
      pg.endShape();
    }
    else if(mode==1 && ps.size()>0){
      pg.beginShape(QUADS);
      pg.texture(img);
      PVector v = ps.get(0);
      pg.vertex(v.x,v.y,v.z, 0, 0);
      v = ps.get(1);
      pg.vertex(v.x,v.y,v.z, img.width, 0);
      v = ps.get(2);
      pg.vertex(v.x,v.y,v.z, img.width, img.height);
      v = ps.get(3);
      pg.vertex(v.x,v.y,v.z, 0, img.height);
      pg.endShape();
    }
    pg.popMatrix();
    
  }

  void update(PVector p1, PVector p2) {
    po.set(p1);
    pf.set(p2);

    dif.set(pf);
    dif.sub(po);
  }

  void update(ArrayList<PVector> p) {
    ps.clear();
    for (PVector v : p) {
      ps.add(v);
    }
  }

  void setMode(int m) {
    mode = m;
  }

  void useFourPoints() {
    setMode(1);
  }

  void useTwoPoints() {
    setMode(0);
  }
}