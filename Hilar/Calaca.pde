class Calaca{
 Bone[][] h;
 int N;
 
 Calaca(){
   h = new Bone[7][15];
   N = 0;
   
   for(int i=0;i<h.length; i++){
  h[i][0] = new Bone("esqueleto_cuello.png");//cuello
  h[i][1] = new Bone("esqueleto_craneo.png");//cabeza
 
  h[i][2] = new Bone("esqueleto_tronco.png");//torso
  h[i][2].useFourPoints();
  h[i][3] = new Bone("esqueleto_brazoD.png");//brazo D  
  h[i][4] = new Bone("esqueleto_antebrazoD.png");// antebrazo D
  h[i][5] = new Bone("esqueleto_manoD.png");// mano D
  
  h[i][6] = new Bone("esqueleto_brazoIz.png");// brazo I
  h[i][7] = new Bone("esqueleto_antebrazoIz.png");// antebrazo I 
  h[i][8] = new Bone("esqueleto_manoIz.png");// mano I
  
  h[i][9] = new Bone("esqueleto_femurD.png");// femur D
  h[i][10] = new Bone("esqueleto_espinillaD.png");// pierna D
  h[i][11] = new Bone("esqueleto_pieD.png");//  pie D
  
  h[i][12] = new Bone("esqueleto_femurIz.png");//femur I
  h[i][13] = new Bone("esqueleto_espinillaIz.png");// pierna I
  h[i][14] = new Bone("esqueleto_pieIz.png");// pie I
   }
 }
 
 void update(ArrayList<PVector> v){
   int j;
   N = v.size()/25;
   for(int i=0; i<N; i++){ // Repeat for each skeleton
   j = i*25; // offset
   ArrayList<PVector> aux = new ArrayList<PVector>();
   h[i][0].update(v.get(j+1),v.get(j+2)); // Cuello: Neck and spine shoulder
   h[i][1].update(v.get(j+0),v.get(j+1)); // Cabeza: Head and Neck
   aux.add(v.get(j+5));
   aux.add(v.get(j+9));
   aux.add(v.get(j+16));
   aux.add(v.get(j+13));
   h[i][2].update(aux); // Torso: shoulder right, left, hip left, hip right
   
   h[i][3].update(v.get(j+5),v.get(j+6)); // Brazo D
   h[i][4].update(v.get(j+6),v.get(j+7)); // Antebrazo D
   h[i][5].update(v.get(j+7),v.get(j+20)); // Mano D
   
   h[i][6].update(v.get(j+9),v.get(j+10)); // Brazo Iz
   h[i][7].update(v.get(j+10),v.get(j+11)); // Antebrazo Iz
   h[i][8].update(v.get(j+11),v.get(j+19)); // Mano Iz
   
   h[i][9].update(v.get(j+13),v.get(j+14)); // Femur D
   h[i][10].update(v.get(j+14),v.get(j+15)); // Pierna D
   h[i][11].update(v.get(j+15),v.get(j+22)); // Pie D
   
   h[i][12].update(v.get(j+16),v.get(j+17)); // Femur Iz
   h[i][13].update(v.get(j+17),v.get(j+18)); // Pierna Iz
   h[i][14].update(v.get(j+18),v.get(j+21)); // Pie Iz
   }
   /* 
    0 KinectPV2.JointType_Head
    1 KinectPV2.JointType_Neck
    2 KinectPV2.JointType_SpineShoulder
    3 KinectPV2.JointType_SpineMid
    4 KinectPV2.JointType_SpineBase
    
    // Right Arm    
   5 KinectPV2.JointType_ShoulderRight
   6 KinectPV2.JointType_ElbowRight
   7 KinectPV2.JointType_WristRight
   8 KinectPV2.JointType_HandRight
    
    // Left Arm
   9 KinectPV2.JointType_ShoulderLeft
   10 KinectPV2.JointType_ElbowLeft
   11 KinectPV2.JointType_WristLeft
   12 KinectPV2.JointType_HandLeft
    
    // Right Leg
    13 KinectPV2.JointType_HipRight
    14 KinectPV2.JointType_KneeRight
    15 KinectPV2.JointType_AnkleRight
    
    // Left Leg
    16 KinectPV2.JointType_HipLeft
    17 KinectPV2.JointType_KneeLeft
    18 KinectPV2.JointType_AnkleLeft
    
    //Joints
    19 KinectPV2.JointType_HandTipLeft
    20 KinectPV2.JointType_HandTipRight
    21 KinectPV2.JointType_FootLeft
    22 KinectPV2.JointType_FootRight
    
    23 KinectPV2.JointType_ThumbLeft
    24 KinectPV2.JointType_ThumbRight*/
   
 }
  
 void draw(PGraphics pg){
   pg.noStroke();
   for(int j=0;j<N;j++){
    for(int i=0;i<h[j].length;i++){
     h[j][i].draw(pg); 
    }
   }
   
 }
  
}