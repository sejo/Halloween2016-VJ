class EscenarioQuads{
  Quad[] q;
  int qptr;
  int size;
  
  int mode;// Mask:0, mapping: 1
  int blend;
  
  EscenarioQuads(){
    size = 8;
   q = new Quad[size]; // 0 is the background
   qptr = 0;
   
   float w = 1100;
   float h = 700;
   float b = w/4;
   float a = h/4;
   q[0] = new Quad(10,10,w,h);
   q[0].setDefaultFill(color(0));
   
   q[1] = new Quad (10+w/9,10, a, b);
   q[2] = new Quad (10+w/3, 10 + h/9, b, a);
   
   q[3] = new Quad(10, 10+h/3+h/9, b, a);
   q[4] = new Quad(10+w/3+w/9, 10+h/3, a, b);
   q[5] = new Quad(10+2*w/3, 10+h/3+h/9, b, a);
   
   q[6] = new Quad(10+w/3, 10+2*h/3+h/9, b, a);
   q[7] = new Quad(10+2*w/3, 10+2*h/3, a, b);
   
   for(int i=1; i<q.length; i++){
    //q[i] = new Quad(10 + (i%4)*250, 50 + (i/4)*300, 200, 200); 
       q[i].setDefaultFill(color(255));
   }

   
  }

  void draw(){

      if(blend==0){
    blendMode(BLEND);
      for(int i=0;i<q.length;i++){
     q[i].draw(); 
     }
  }
  else if(blend==7){
    for(int i=1;i<q.length;i++){
     q[i].draw(); 
     }
    blendMode(MULTIPLY);
    q[0].draw();
    blendMode(BLEND);
}

  
  }
  
  void setMappingMode(){
       for(int i=1;i<8;i++){
        setEnTexture(i,true);
        }
        setEnTexture(0,false);
      setBlendNormal();
    
  }
  
  void setMaskMode(){
       for(int i=1;i<8;i++){
          setEnTexture(i,false);
        }
      setBlendMultiply();
    
  }

  void setBlendMultiply(){
  	blend = 7;
  setEnTexture(0,true);
  }
  void setBlendNormal(){
  	blend = 0;
  }
  
  void setTexture(int index, PGraphics t){
    q[index].setTexture(t);
  }

  boolean toggleEnTexture(int index ){
    q[index].toggleEnTexture();
    return q[index].getEnTexture();
  }

  void setEnTexture(int index, boolean t){
	q[index].setEnTexture(t);
  }
  
  void drag(boolean pressed, float x, float y){
    q[qptr].drag(pressed, x, y);
  }
  
  int rotateActive(){
     qptr = (qptr+1)%size; 
     return qptr;
  }

  boolean toggleShowVertex(){
	for(int i=0; i<q.length;i++){
		q[i].toggleShowVertex();
    	}
  return q[0].getShowVertex();  
}

void saveVertices(){
  ArrayList<PVector> vertices = new ArrayList<PVector>();

  for(int i=0;i<q.length;i++){
      ArrayList<PVector> aux = q[i].getVertices();
      for(PVector vec : aux){
       vertices.add(vec); 
      }
  }
  String[] text = new String[vertices.size()*3];
  PVector vc;
  for(int i=0;i<vertices.size();i++){
   text[i*3] = str(vertices.get(i).x);
   text[i*3+1] = str(vertices.get(i).y); 
   text[i*3+2] = str(vertices.get(i).z); 
  }
  
  saveStrings("vertices.txt",text);
}

void loadVertices(){
 String[] text = loadStrings("vertices.txt");
 PVector vc = new PVector();
 ArrayList<PVector> list = new ArrayList<PVector>();
 for(int i=0;i<text.length/3;i++){
  list.add(new PVector(float(text[i*3]),float(text[i*3+1]),float(text[i*3+2]))); 
 }
 ArrayList<PVector> aux = new ArrayList<PVector>();
 for(int i=0; i<list.size()/4;i++){
  aux.clear();
  for(int j=0;j<4;j++){
   aux.add(list.get(i*4 + j)); 
  }
  q[i].setVertices(aux);
 }
  
}

  void receiveKey(int k){
	switch(k){
		case 'J':
			rotateActive();
			break;
		case 'V':
			toggleShowVertex();
			break;
		case 't':
			toggleEnTexture(qptr);
			break;

		case 'j':
			q[qptr].rotateActive();
		break;
		case 'a':
			q[qptr].decX();
		break;
		case 's':
			q[qptr].incY();
		break;
		case 'd':
			q[qptr].incX();
		break;
		case 'w':
			q[qptr].decY();
		break;

          // Save and load vertices
          case 'G':
            saveVertices();
            break;
          case 'L':
            loadVertices();
            break;

		/*
		case 't':
			q[qptr].toggleEnTexture();
		break;
		case 'T':
			q[qptr].toggleShowVertex();
		break;
		*/


	}

  }
  
}