import java.nio.*;
import KinectPV2.*;
import processing.video.*;

//Kinect
KinectPV2 kinect;
KinectPointCloud kpc;
KinectSkeletonDepth ksd;
int maxD = 4500; // mm
int minD = 0;  //  mm

// Escenario
EscenarioQuads es;
//Escenas
pgEscena pc;
pgEscena ar;
int scene;

Background bg;

int mode; // Mask:0, mapping: 1
PGraphics p;
PGraphics b;

int blend;

void setup(){

  fullScreen(P3D);
  
  // Kinect
  kinect = new KinectPV2(this);
    kinect.enableDepthImg(true);
  kinect.enablePointCloud(true);
   kinect.enableSkeletonDepthMap(true);
  kinect.init();
  kpc = new KinectPointCloud();
  kpc.setup();
  ksd = new KinectSkeletonDepth();
  ksd.setup();

  // Escenario
  es = new EscenarioQuads();

  // Escenas
  pc = new scPointCloud();
  pc.setup(kinect.WIDTHDepth, kinect.HEIGHTDepth);
  
  ar = new scArena();
  ar.setup(kinect.WIDTHDepth, kinect.HEIGHTDepth);
  
  mode = 1;
  es.setMappingMode();
  
  bg = new Background(this);
  
  key = 'm';
  keyPressed(); // Mapping do

  p = createGraphics(100,100);
  p.beginDraw();
  p.background(255,0,0);
  p.endDraw();
  
  b = createGraphics(p.width,p.height,P3D);
  b.beginDraw();
  b.background(0,255,0);
  b.endDraw();
  
  blend = 0;
}

void draw(){  
  kinect.setLowThresholdPC(minD);
  kinect.setHighThresholdPC(maxD);
  
  blendMode(BLEND);
  background(0,0,0);
  
  switch(scene){
    case 0: // Point Cloud
      kpc.update();
      pc.update(kpc.getPoints());
      pc.draw();
      p = pc.getPgAt(0);
    break;
    
    case 1: // Arena  
    ksd.update();
    ar.update(ksd.getPoints());
    ar.draw();
    p = ar.getPgAt(0);
    break;
    
    case 100:
    break;
  }
  
  b = createGraphics(p.width,p.height,P3D);
  b.beginDraw();
  b.blendMode(BLEND);
  bg.draw(b);
  switch(blend){
    case 0:
      b.blendMode(ADD);
      break;
    case 1:
      b.blendMode(EXCLUSION);
      break;
    case 2:
      b.blendMode(MULTIPLY);
      break;
     default:
       b.blendMode(ADD);
      break;
  
  }
  b.image(p,0,0);
  b.endDraw();

  
  switch(mode){
    case 0: // Mask
          es.setTexture(0,b);
    break;
    case 1: // Mapping
        for(int i=0;i<8;i++){
          es.setTexture(i,b); 
        }    
    break;
  }
  
  blendMode(BLEND);

  es.drag(mousePressed,mouseX,mouseY);
  es.draw();
 /* 
  fill(255);
  text("Frame rate: "+frameRate,50,50);
 text("MinD: "+minD+" MaxD: "+maxD,50,80);
*/

}

public void keyPressed() {
  switch(scene){
    case 0:
      pc.receiveKey(key);
      break;
    case 1:
      ar.receiveKey(key);
      break;
      
  }

  es.receiveKey(key);
  
  switch(key){
   case '1':
     scene = 0;
     break;
   case '2':
     scene = 1;
     break;
   case 'm':
     scene = 100;
     /*
       for(int i=0;i<8;i++){
        es.setEnTexture(i,true);
        }
       
    es.setBlendNormal();*/
     break;
     
   case 'n':
     bg.nextMovie();
     break;
     
   case 'N':
     bg.prevMovie();
     break;
     
   case 'b':
     bg.toggleEnabled();
     break;
     
   case '<':
     blend = (blend+1)%3;
     break;
     
  case '¿': // Toggle mode
    if(mode==0){
      mode = 1; // Mapping
      es.setMappingMode();
    }
    else{
      mode = 0; // Mask
      es.setMaskMode();
    }
    break;
    
    
  }

  if (key == '!') {
    minD += 100;
    println("Change min: "+minD);
  }

  if (key == '"') {
    minD -= 100;
    println("Change min: "+minD);
  }

  if (key == '#') {
    maxD += 100;
    println("Change max: "+maxD);
  }

  if (key == '$') {
    maxD -= 100;
    println("Change max: "+maxD);
  }
}

void movieEvent(Movie m){
 m.read(); 
}