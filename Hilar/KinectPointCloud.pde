class KinectPointCloud{
  
  /* Requires in setup
      kinect.enableDepthImg(true);
  kinect.enablePointCloud(true); */
  
  float maxx, minx, maxy, miny, maxz, minz;
  ArrayList<PVector> ve;
  
  
  void setup(){
  ve = new ArrayList<PVector>();
  maxx=1;
  minx=0;
  maxy=1;
  miny=0;
  maxz=1;
  minz=0;
    
  }
  
  void update(){
               ve.clear();
   FloatBuffer pointCloudBuffer = kinect.getPointCloudDepthPos();
   maxx =  pointCloudBuffer.get(0);
  minx = maxx;
  
    maxy =  pointCloudBuffer.get(1);
  miny = maxy;
  
    maxz =  pointCloudBuffer.get(2);
  minz = maxz;
  

  for(int i=0;i<kinect.WIDTHDepth*kinect.HEIGHTDepth;i++){
    float x = pointCloudBuffer.get(i*3 + 0);
    float y = pointCloudBuffer.get(i*3 + 1);
    float z = pointCloudBuffer.get(i*3 + 2);
    if(z>0){
    ve.add(new PVector(x,y,z));
    }
    //point(x,y,z);

     if(x > maxx){
       maxx = x;
     }
     if(x < minx){
       minx = x;
     }
     
          if(z > maxz){
       maxz = z;
     }
     if(z < minz){
       minz = z;
     }
     
     if(y > maxy){
       maxy = y;
     }
     if(y < miny){
       miny = y;
     }   
  }
  ve.add(new PVector(minx,miny,minz));
  ve.add(new PVector(maxx, maxy, maxz)); 
  }
  
  ArrayList<PVector> getPoints(){
   return ve; 
  }
}