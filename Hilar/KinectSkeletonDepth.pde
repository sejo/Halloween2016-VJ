class KinectSkeletonDepth{
    ArrayList<PVector> ve;
    KJoint[] joints;
    
    void setup(){
       ve = new ArrayList<PVector>();
    }
 
  
  void update(){
    ve.clear();
    
     //get the skeletons as an Arraylist of KSkeletons
  ArrayList<KSkeleton> skeletonArray =  kinect.getSkeletonDepthMap();

  //individual joints
  for (int i = 0; i < skeletonArray.size(); i++) {
    KSkeleton skeleton = (KSkeleton) skeletonArray.get(i);
    //if the skeleton is being tracked compute the skleton joints
    if (skeleton.isTracked()) {
       joints = skeleton.getJoints();

  //    color col  = skeleton.getIndexColor();
 //     fill(col);
 //     stroke(col);
             
    addJoint(KinectPV2.JointType_Head);
    addJoint(KinectPV2.JointType_Neck);
    addJoint(KinectPV2.JointType_SpineShoulder);
    addJoint(KinectPV2.JointType_SpineMid);
   addJoint(KinectPV2.JointType_SpineBase);
    
    // Right Arm    
       addJoint(KinectPV2.JointType_ShoulderRight);
      addJoint( KinectPV2.JointType_ElbowRight);
      addJoint( KinectPV2.JointType_WristRight);
       addJoint(KinectPV2.JointType_HandRight);
    
    // Left Arm
       addJoint(KinectPV2.JointType_ShoulderLeft);
      addJoint( KinectPV2.JointType_ElbowLeft);
       addJoint(KinectPV2.JointType_WristLeft);
      addJoint( KinectPV2.JointType_HandLeft);
    
    // Right Leg
      addJoint( KinectPV2.JointType_HipRight);
       addJoint(KinectPV2.JointType_KneeRight);
      addJoint( KinectPV2.JointType_AnkleRight);
    
    // Left Leg
       addJoint(KinectPV2.JointType_HipLeft);
       addJoint(KinectPV2.JointType_KneeLeft);
       addJoint(KinectPV2.JointType_AnkleLeft);
    
    //Joints
       addJoint(KinectPV2.JointType_HandTipLeft);
      addJoint( KinectPV2.JointType_HandTipRight);
      addJoint( KinectPV2.JointType_FootLeft);
      addJoint( KinectPV2.JointType_FootRight);
    
      addJoint( KinectPV2.JointType_ThumbLeft);
      addJoint( KinectPV2.JointType_ThumbRight);
    }
  }
  }
 
   ArrayList<PVector> getPoints(){
   return ve; 
  }
  
  void addJoint(int jointType){
   ve.add(new PVector(joints[jointType].getX(), joints[jointType].getY(), joints[jointType].getZ()));
  }
    
  
  
}