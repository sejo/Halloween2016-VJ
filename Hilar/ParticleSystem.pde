class ParticleSystem{
 ArrayList<PVector> loc, vel, acc;
 ArrayList<Integer>life;
 float w, h; // Size of drawing
 float psize;
 int maxlife;
 
 int mode; // 0 is restarting when out of limits, 1 is hitting floor and disappearing, 2 is with life
 
 ParticleSystem(float _w, float _h){
  loc = new ArrayList<PVector>();
  vel = new ArrayList<PVector>();
  acc = new ArrayList<PVector>();
  life = new ArrayList<Integer>();
  maxlife = 200;
  w=_w;
  h=_h;
  mode = 2;
  
  psize = 10;
   
 }
 
 void setRandom(){
  ArrayList<PVector> ran = new ArrayList<PVector>();
  for(int i=0;i<300; i++){
   ran.add(new PVector(random(w),random(h),random(10))); 
  }
  setLoc(ran);
   
 }
 
 void addParticle(PVector l){
  loc.add(l);
  vel.add(new PVector(random(-5,5),random(-5,5),0));
  acc.add(new PVector(0, 0, 0));
  life.add(maxlife);
 }
 
  void addParticleExploding(PVector l){
  loc.add(l);
  vel.add(new PVector(random(-5,5),random(-5,5),0));
  acc.add(new PVector(0, 0, 0));
  life.add(maxlife);
 }
 
  void addParticleFountain(PVector l){
  loc.add(l);
  vel.add(new PVector(random(-1,1),random(-5,0),0));
  acc.add(new PVector(0, 0, 0));
  life.add(maxlife);
 }
 
 void addParticleString(PVector l){
  loc.add(l);
  vel.add(new PVector(random(-1,1),0,0));
  acc.add(new PVector(0, 0, 0));
  life.add(maxlife);
 }
 
 void setLoc(ArrayList<PVector> v){ // Sets the locations with a given arraylist
   loc.clear();
   vel.clear();
   acc.clear();
   for(int i=0;i<v.size();i++){
       PVector vec = v.get(i);
       loc.add(new PVector(vec.x, vec.y, vec.z));
       vel.add(new PVector(0,random(-10),0));
       acc.add(new PVector(0,0,0));
       life.add(maxlife);
   }
 }
 
 void addForce(PVector f){
  for(PVector a : acc){
   a.add(f); 
  }
 }
 
 void update(){
   PVector l, v,a;
   for(int i=0;i<loc.size();i++){
      l = loc.get(i);
      v = vel.get(i);
      a = acc.get(i);
      v.add(a);
      l.add(v);
      a.set(0,0,0);
      life.set(i, life.get(i)-1);
      

      switch(mode){
       case 0:
         if(l.y>h){
            l.y = random(h); 
            v.set(0,random(10),0);
         }
         break;
       case 1:
         if(l.y>h){
            loc.remove(i);
            vel.remove(i);
            acc.remove(i);
         }
        break;
       case 2:
         if(life.get(i)==0){
            loc.remove(i);
            vel.remove(i);
            acc.remove(i);
            life.remove(i);
         }
      }
   }
   
 }
 
 void setMode(int m){
    mode = m; 
 }
 
 void setMaxLife(int m){
    maxlife = m; 
 }
 
 void setPSize(float s){
  psize = s; 
 }
 
 void draw(PGraphics pg){
   pg.stroke(255,255,255,200);
   pg.strokeWeight(psize);
   PVector l;
  for(int i=0;i<loc.size(); i++){
    l = loc.get(i);
    if(mode==2){
     pg.stroke(255,255,255,180.0*life.get(i)/maxlife); 
    }
    pg.point(l.x, l.y, l.z);
  }
   
   
 }
  
  
  
}