class Quad{
	PVector[] v;
	PGraphics tex;
	int aV;
	boolean enTexture;
	boolean showVertex;
	color defaultFill;
boolean dragged;


	Quad(float x, float y, float w, float h){
		this(new PVector(x,y), new PVector(x+w,y), new PVector(x+w,y+h), new PVector(x,y+h));
	}

	Quad(PVector v0, PVector v1, PVector v2, PVector v3){
		v = new PVector[4];
		v[0] = new PVector(v0.x,v0.y);
		v[1] = new PVector(v1.x,v1.y);
		v[2] = new PVector(v2.x,v2.y);
		v[3] = new PVector(v3.x,v3.y);

		tex = createGraphics(600,400);

		enTexture = false;
		showVertex = true;
		aV = 0;
		defaultFill = color(255);
          dragged = false;
	}

	Quad(){
		this(new PVector(0,0), new PVector(100,0), new PVector(100,100), new PVector(0,100));
	}

	void draw(){
		fill(defaultFill);
		beginShape(QUADS);
		if(enTexture){
			texture(tex);
		}
		vertex(v[0].x,v[0].y,0,0);
		vertex(v[1].x,v[1].y,tex.width,0);
		vertex(v[2].x,v[2].y,tex.width,tex.height);
		vertex(v[3].x,v[3].y,0,tex.height);
		endShape();

		if(showVertex){
			noFill();
			stroke(200,200,0);
			strokeWeight(2);
			float d=20;
			ellipse(v[aV].x,v[aV].y,d,d);
			line(v[aV].x-d/2,v[aV].y,v[aV].x+d/2,v[aV].y);
			line(v[aV].x,v[aV].y-d/2,v[aV].x,v[aV].y+d/2);
		}
		else{
			noStroke();
		}

	}

	float incX(){ // Increase X of active vertex
		v[aV].x += 1;
		return v[aV].x;
	}

	float incY(){ // Increase Y of active vertex
		v[aV].y += 1;
		return v[aV].y;
	}

	float decX(){ // Decrease X of active vertex
		v[aV].x -= 1;
		return v[aV].x;
	}

	float decY(){ // Decrease Y of active vertex
		v[aV].y -= 1;
		return v[aV].y;
	}

	void setTexture(PGraphics t){
		tex = t;
	}

	int rotateActive(){
		aV = (aV+1)%4;
		return aV;

	}


	boolean toggleEnTexture(){
		enTexture = !enTexture;
		return enTexture;
	}

	boolean toggleShowVertex(){
		showVertex = !showVertex;
		return showVertex;
	}

	void setEnTexture(boolean t){
		enTexture = t;
	}
	void setShowVertex(boolean t){
		showVertex = t;
	}

  boolean getEnTexture(){
       return enTexture; 
  }
  
  boolean getShowVertex(){
     return showVertex; 
  }


	void setDefaultFill(color f){
		defaultFill = f;
	}

	void drag(boolean pressed, float x, float y){
		if(pressed){
			PVector p = v[aV];
			if(x<p.x+20 && x>p.x-20 && y<p.y+20 && y>p.y-20){
                          dragged = true;

			}
		}
          else{
           dragged = false; 
          }
          
          if(dragged){
                v[aV].x = x;
                v[aV].y = y;
          }

	}

  ArrayList<PVector> getVertices(){
    ArrayList<PVector> vr = new ArrayList<PVector>();
    for(int i=0;i<v.length;i++){
       vr.add(v[i]); 
    }
    return vr;
  }
  
  void setVertices(ArrayList<PVector> vr){
   for(int i=0;i<vr.size();i++){
    v[i] = vr.get(i); 
   }
    
  }

}