public interface pgEscena{
 //PGraphics[] pg; // Your class should have this variable declared
 
 void setup(int h, int v); 
 /* This method will be called in the main setup() function 
   What should be done inside this method:
     * Do the setup that your pgEscena needs
     * Setup the pg array with a size of 1 or 7.
     * If the array has a size of 7:
     * Create the corresponding PGraphics in each element of the array, according to the following proportions:
       0: Vertical
       1: Horizontal
       2: Horizontal
       3: Vertical
       4: Horizontal
       5: Horizontal
       6: Vertical
       with the dimensions given by h and v:
         pg[0] = createGraphics(v, h); // Vertical rectangle
         pg[1] = createGraphics(h, v); // Horizontal rectangle
         and so on
     * If the array has a size of 1, create an horizontal PGraphics with its dimensions given by h and v
 */
 
 void update(ArrayList<PVector> v);
 /* This method will be called in the draw() loop when there are new inputs.
     Inputs are given in an ArrayList of 3D PVectors (with x, y and z) and can come from many sources.
     
     When using KinectPV2, we are going to expect an ArrayList of size 25 
       with each PVector corresponding to a bone or joint in the following order 
       (based on the KinectPV2 / Skeleton3d example):
       
     0 KinectPV2.JointType_Head
    1 KinectPV2.JointType_Neck
    2 KinectPV2.JointType_SpineShoulder
    3 KinectPV2.JointType_SpineMid
    4 KinectPV2.JointType_SpineBase
    
    // Right Arm    
   5 KinectPV2.JointType_ShoulderRight
   6 KinectPV2.JointType_ElbowRight
   7 KinectPV2.JointType_WristRight
   8 KinectPV2.JointType_HandRight
    
    // Left Arm
   9 KinectPV2.JointType_ShoulderLeft
   10 KinectPV2.JointType_ElbowLeft
   11 KinectPV2.JointType_WristLeft
   12 KinectPV2.JointType_HandLeft
    
    // Right Leg
    13 KinectPV2.JointType_HipRight
    14 KinectPV2.JointType_KneeRight
    15 KinectPV2.JointType_AnkleRight
    
    // Left Leg
    16 KinectPV2.JointType_HipLeft
    17 KinectPV2.JointType_KneeLeft
    18 KinectPV2.JointType_AnkleLeft
    
    //Joints
    19 KinectPV2.JointType_HandTipLeft
    20 KinectPV2.JointType_HandTipRight
    21 KinectPV2.JointType_FootLeft
    22 KinectPV2.JointType_FootRight
    
    23 KinectPV2.JointType_ThumbLeft
    24 KinectPV2.JointType_ThumbRight
 
 */
 
 void draw();
 /* This method will draw in the pg array elements. It will be called every time in the main draw() loop
 */

  void receiveKey(int k);
 /* This method can be thought as the keyPressed(int key) method: 
 switch variable k to modify parameters or start actions in your class.
 */
 
 PGraphics getPgAt(int index);
 /* This method must return the PGraphics corresponding to the given index in the pg array.
   It should be implemented in the following way (copy and paste the implementation in your own class)
  PGraphics getPgAt(int index){
   if(pg.length==7 && index>= 0 && index<7){
    return pg[index]; // Return the corresponding element if the array has a size of 7 and if the index is valid
   }
   else{
    return pg[0]; // In any other case return the array element that should always exist
   }
   
 }
 */
 
}