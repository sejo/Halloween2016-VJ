class scArena implements pgEscena{
  PGraphics[] pg; // Your class should have this variable declared
   ArrayList<PVector> ve;
   float scaling;
   
   float angle;
   
   boolean doArena;
   boolean doEmitParticles;
   
   boolean doFade;
   
   boolean doCalaca;
   
   int particlesMode;
 
 ParticleSystem ps;
 
 Calaca c;
 
 void setup(int h, int v){
    ve = new ArrayList<PVector>();
  pg = new PGraphics[1];
  pg[0] = createGraphics(h, v, P3D); 
    scaling = 1;
    
    
  ps = new ParticleSystem(h,v);
  doArena = false;
  doEmitParticles = false;
  doFade = false;
  particlesMode = 0;
  
  angle =0;

  c = new Calaca();
  doCalaca = false;
   
 }
 /* This method will be called in the main setup() function 
   What should be done inside this method:
     * Do the setup that your pgEscena needs
     * Setup the pg array with a size of 1 or 7.
     * If the array has a size of 7:
     * Create the corresponding PGraphics in each element of the array, according to the following proportions:
       0: Vertical
       1: Horizontal
       2: Horizontal
       3: Vertical
       4: Horizontal
       5: Horizontal
       6: Vertical
       with the dimensions given by h and v:
         pg[0] = createGraphics(v, h); // Vertical rectangle
         pg[1] = createGraphics(h, v); // Horizontal rectangle
         and so on
     * If the array has a size of 1, create an horizontal PGraphics with its dimensions given by h and v
 */
 
 void update(ArrayList<PVector> v){
  this.ve = v; 
  for(int i=0;i<ve.size();i++){
   ve.get(i).x = kinect.WIDTHDepth-ve.get(i).x; 
  }
 }
 /* This method will be called in the draw() loop when there are new inputs.
     Inputs are given in an ArrayList of 3D PVectors (with x, y and z) and can come from many sources.
     
     When using KinectPV2, we are going to expect an ArrayList of size 27 
       with each PVector corresponding to a bone or joint in the following order 
       (based on the KinectPV2 / Skeleton3d example):
       
    KinectPV2.JointType_Head
    KinectPV2.JointType_Neck
    KinectPV2.JointType_SpineShoulder
    KinectPV2.JointType_SpineMid
    KinectPV2.JointType_SpineBase
    
    // Right Arm    
    KinectPV2.JointType_ShoulderRight
    KinectPV2.JointType_ElbowRight
    KinectPV2.JointType_WristRight
    KinectPV2.JointType_HandRight
    KinectPV2.JointType_WristRight
    
    // Left Arm
    KinectPV2.JointType_ShoulderLeft
    KinectPV2.JointType_ElbowLeft
    KinectPV2.JointType_WristLeft
    KinectPV2.JointType_HandLeft
    KinectPV2.JointType_WristLeft
    
    // Right Leg
    KinectPV2.JointType_HipRight
    KinectPV2.JointType_KneeRight
    KinectPV2.JointType_AnkleRight
    
    // Left Leg
    KinectPV2.JointType_HipLeft
    KinectPV2.JointType_KneeLeft
    KinectPV2.JointType_AnkleLeft
    
    //Joints
    KinectPV2.JointType_HandTipLeft
    KinectPV2.JointType_HandTipRight
    KinectPV2.JointType_FootLeft
    KinectPV2.JointType_FootRight
    
    KinectPV2.JointType_ThumbLeft
    KinectPV2.JointType_ThumbRight
 
 */
 
 void draw(){
   angle += 0.01;
   if(doArena){
     ps.addForce(new PVector(0,0.1,0)); // gravity
   }
   if(doEmitParticles){
     ps.addForce(new PVector(0.2*(noise(angle+2000)-0.5),0.2*(noise(angle)-0.5),0)); // gravity
   }
   
    pg[0].beginDraw();
    if(doFade){
     pg[0].fill(255,0,0,1);
     pg[0].noStroke();
     pg[0].rect(0,0,pg[0].width,pg[0].height); 
    }
    else{
      pg[0].clear();
    }
    pg[0].stroke(255,255,255,250);
    pg[0].strokeWeight(5);
    pg[0].fill(255,255,255,250);
    pg[0].scale(scaling);
    PVector v;
    for(int i=0; i<ve.size(); i++){
          pg[0].pushMatrix();
        v = ve.get(i);
        if(doEmitParticles){
          switch(particlesMode){
            case 0:
               ps.addParticleFountain(v); 
               break;
            case 1:
              ps.addParticleExploding(v);
              break;
            case 2:
              ps.addParticleString(v);
              break;
          }
        }
      pg[0].translate(v.x,v.y,v.z);
   //   pg[0].ellipse(0,0,10,10);
          pg[0].popMatrix();
          if(i<ve.size()-7 && !doCalaca){
           pg[0].line(v.x, v.y, v.z, ve.get(i+1).x, ve.get(i+1).y, ve.get(i+1).z);
          }
    }
    if(doCalaca){
     c.update(ve);
     c.draw(pg[0]);
    }
    if(doArena || doEmitParticles){
         ps.update();
         ps.draw(pg[0]);
    }
    pg[0].endDraw(); 
 }
 /* This method will draw in the pg array elements. It will be called every time in the main draw() loop
 */

  void receiveKey(int k){
    switch(k){
     case '+':
       scaling += 0.1;
       break;
     case '*':
       scaling -= 0.1;
       break;
     case 'A':
       doArena = !doArena;
       if(doArena){
         ps.setRandom();
         ps.setMode(0); // Restarting
       }
       break;
     case 'E':
       doEmitParticles = !doEmitParticles;
       if(doEmitParticles){
        ps.setMode(2);
       }
       break;
     case 'e':
       particlesMode = (particlesMode +1)%3;
       break;
      case 'R':
        doFade = !doFade;
        break;
      case 'C':
        doCalaca = !doCalaca;
        break;
      
    }
    
  }
 /* This method can be thought as the keyPressed(int key) method: 
 switch variable k to modify parameters or start actions in your class.
 */
 
 
  PGraphics getPgAt(int index){
   if(pg.length==7 && index>= 0 && index<7){
    return pg[index]; // Return the corresponding element if the array has a size of 7 and if the index is valid
   }
   else{
    return pg[0]; // In any other case return the array element that should always exist
   }
   
 }
 
  
}