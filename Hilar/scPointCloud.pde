class scPointCloud implements pgEscena{
 PGraphics[] pg; // Your class should have this variable declared
 float mapz;
 float maxx, minx, maxy, miny, maxz, minz;
   PVector v;
  float px, py, pz;
  float count;
 ArrayList<PVector> ve;
 float pointSize;
 float pointAlpha;
 float countIncrease;
 float radius;
 int axisRotation;
 
 boolean constantBackground;
 int frameCountTarget;
 
 boolean pause;
 
  ParticleSystem ps;
 
 void setup(int h, int v){
 /* This method will be called in the main setup() function 
   What should be done inside this method:
     * Do the setup that your pgEscena needs
     * Setup the pg array with a size of 1 or 7.
     * If the array has a size of 7:
     * Create the corresponding PGraphics in each element of the array, according to the following proportions:
       0: Vertical
       1: Horizontal
       2: Horizontal
       3: Vertical
       4: Horizontal
       5: Horizontal
       6: Vertical
       with the dimensions given by h and v:
         pg[0] = createGraphics(v, h); // Vertical rectangle
         pg[1] = createGraphics(h, v); // Horizontal rectangle
         and so on
     * If the array has a size of 1, create an horizontal PGraphics with its dimensions given by h and v
 */
   mapz = 1000;
    maxx=1;
  minx=0;
  maxy=1;
  miny=0;
  maxz=1;
  minz=0;
  
  ve = new ArrayList<PVector>();
  pg = new PGraphics[1];
  pg[0] = createGraphics(h, v, P3D);
  
  count = 0;
  pointSize = 1;
  
  pointAlpha = 100;
  
  countIncrease = 0;
  axisRotation = 0;
  
  frameCountTarget = 30;
  
  constantBackground = true;
  
  
  radius = 1000;
  
  pause = false;
  ps = new ParticleSystem(h,v);
  ps.setMode(2);
  ps.setMaxLife(200);
 
 }
 
 void update(ArrayList<PVector> ve){
    this.ve = ve;
    
 
 }
 
 void draw(){
   pg[0].beginDraw();
   switch(axisRotation){
     case 0:
        pg[0].camera(radius*sin(count)+pg[0].width/2,pg[0].height/2, radius*cos(count), pg[0].width/2, pg[0].height/2, 0, 0, 1, 0);
        break;
     case 1:
         pg[0].camera(pg[0].width/2,radius*sin(count)+pg[0].height/2, radius*cos(count), pg[0].width/2, pg[0].height/2, 0, 0, 0, 1);
         break;
   }
   if(constantBackground){
     pg[0].clear();
  // pg[0].background(0);
   }
   else{
    if(frameCount%frameCountTarget == 0){
       pg[0].clear();
       frameCountTarget = int(random(20,50));
    }
   }
   pg[0].lights();

  pg[0].strokeWeight(pointSize);
  pg[0].stroke(255,255,255,pointAlpha);
   
  if(pause){
   ps.addForce(new PVector(0,0.9,0));
   ps.update(); 
   ps.draw(pg[0]);
  }
  else{
  
    minx = ve.get(ve.size()-2).x;
    miny = ve.get(ve.size()-2).y;
    minz = ve.get(ve.size()-2).z;
    maxx = ve.get(ve.size()-1).x;
    maxy = ve.get(ve.size()-1).y;
    maxz = ve.get(ve.size()-1).z;
  for(int i=0;i<ve.size()-2;i++){
      v = ve.get(i);
      px = map(v.x, minx, maxx, 0, pg[0].width);
      py = map(-v.y, -maxy, -miny, 0, pg[0].height);
      pz = map(v.z, minz, maxz, -mapz, mapz);
      //pz += (mapz/10)*noise(count + i*0.01)-mapz/5;
      pg[0].point(px,py,pz);
    //  line(px,py,pz,px,1000,pz);
    // line(px,py,pz,1000,px,pz);
     //line(px,py,pz,px,py,1000);
    /* if(i<ve.size()-2-848 && (i%848==0)){
       v = ve.get(i+848);
      pg[0].line(px,py,pz, map(v.x, minx, maxx, 0, pg[0].width),  map(-v.y, -maxy, -miny, 0, pg[0].height), map(v.z, minz, maxz, -mapz, mapz));
     }*/
  }
  
  }

  
  pg[0].endDraw();
   count += countIncrease;
 }


  void receiveKey(int k){
    switch(k){
     case 'i':
       pointSize -= 0.5;
       break;
     case 'o':
       pointSize += 0.5;
       break;
     case 'I':
       pointAlpha -= 10;
       break;
     case 'O':
       pointAlpha += 10;
       break;
     case '8':
       countIncrease -= 0.01;
       break;
     case '9':
       countIncrease += 0.01;
       break;
     case '(':
     
       axisRotation = axisRotation==1? 0 : 1;
       break;
     case 'R':
       constantBackground = !constantBackground;
       break;
       
     case '+':
       radius -= 10;
       break;
     case '*':
       radius += 10;
       break;
       
     case 'p':
       pause = !pause;
       if(pause){
         ArrayList<PVector> aux = new ArrayList<PVector>();
         
             minx = ve.get(ve.size()-2).x;
    miny = ve.get(ve.size()-2).y;
    minz = ve.get(ve.size()-2).z;
    maxx = ve.get(ve.size()-1).x;
    maxy = ve.get(ve.size()-1).y;
    maxz = ve.get(ve.size()-1).z;
  for(int i=0;i<ve.size()-2;i++){
      v = ve.get(i);
      px = map(v.x, minx, maxx, 0, pg[0].width);
      py = map(-v.y, -maxy, -miny, 0, pg[0].height);
      pz = map(v.z, minz, maxz, -mapz, mapz);
      //pz += (mapz/10)*noise(count + i*0.01)-mapz/5;
      aux.add(new PVector(px,py,pz));
    //  line(px,py,pz,px,1000,pz);
    // line(px,py,pz,1000,px,pz);
     //line(px,py,pz,px,py,1000);
    /* if(i<ve.size()-2-848 && (i%848==0)){
       v = ve.get(i+848);
      pg[0].line(px,py,pz, map(v.x, minx, maxx, 0, pg[0].width),  map(-v.y, -maxy, -miny, 0, pg[0].height), map(v.z, minz, maxz, -mapz, mapz));
     }*/
  }
         ps.setLoc(aux);
         ps.setPSize(pointSize);
       }
       break;
    }
    
  }
 /* This method can be thought as the keyPressed(int key) method: 
 switch variable k to modify parameters or start actions in your class.
 */
 

  PGraphics getPgAt(int index){
   if(pg.length==7 && index>= 0 && index<7){
    return pg[index]; // Return the corresponding element if the array has a size of 7 and if the index is valid
   }
   else{
    return pg[0]; // In any other case return the array element that should always exist
   }
   
 }
 
  
  
}