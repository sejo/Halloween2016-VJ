class Carrerita implements Scene{
	int w, h, cw, ch;

	boolean selected;

	boolean playing;

	PImage im;

	PGraphics mask;
	PShader shader;

	int counter, framecounter;
	int frameperiod;


	int N,M;

	Carrerita(){
		selected = false;
		counter = 0;

		N = 16;
		M = 21;

		frameperiod = 1; 

		shader = loadShader("texfrag_carrerita.glsl");
	}

	void setup(int w_, int h_, int cw_, int ch_){
		w = w_;
		h = h_;
		cw = cw_;
		ch = ch_;

		im = createImage(cw,ch,RGB);

		mask = createGraphics(1,ch,P3D); // Vertical
//		mask = createGraphics(cw,1,P3D); // Horizontal


	}

	void setFrame(PImage img){
//		im.set(0,0,img);
		im = img; // Con la cámara así

	}

	void update(){
		if(selected && playing){
			if(framecounter%frameperiod == 0){
				mask.beginDraw();
				mask.clear();
				mask.background(0);
				int he;
				mask.stroke(255);
				mask.strokeWeight(3);
				for(int i=0;i<N;i++){
				he = int(random(mask.height));
				mask.line(0,he,1,he);
				/*
				he = int(random(mask.width));
				mask.line(he,0,he,1);
				*/
				}
				mask.endDraw();
				shader.set("mask",mask);
				counter++;
			}

			framecounter++;
		}
	}

	void draw(PGraphics pg){
		if(selected && playing){



		pg.beginDraw();
		pg.shader(shader);
		pg.noStroke();
		pg.beginShape(QUADS);
		pg.texture(im);
		pg.vertex(0,0, 0,0);
		pg.vertex(w,0, cw,0);
		pg.vertex(w,h, cw,ch);
		pg.vertex(0,h, 0,ch);
		pg.endShape();
		pg.endDraw();
		}
          else{
            pg.beginDraw();
            pg.background(0);
            pg.endDraw();
          }
	}

	void receiveKey(int k){
		if(k=='5'){
			selected = !selected;
		}
		if(selected){
			switch(k){
				case ' ':
					playing = !playing;
					break;
                          case 'j':
                            if(N>2){
                             N--; 
                            }
                            println("N "+N);
                            break;
                          case 'k':
                            N++;
                            println("N "+N);
                            break;

			}
		}

	}


	boolean isSelected(){
		return selected;
	}


}
