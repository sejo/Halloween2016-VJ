class Conexiones implements Scene{
	int w, h, cw, ch;

	boolean selected;

	boolean playing;

	boolean store;

	PImage im;

	PGraphics mask;
	PShader shader;

	int counter, framecounter;
	int frameperiod;
	

	PGraphics [] imgs;
	int ptr;

	PGraphics imag;
  PGraphics negro;
  
	int N,M;

	int millis_begin;

	int NperBeat;

	int rcw, rch;

	int dif;

	int lastMillis;
   boolean drawCam;
   int version;

	TimedEventGenerator timer;

	Conexiones(PApplet pa){
		selected = false;
		counter = 0;

		N = 20;

		frameperiod = 10; 

		shader = loadShader("texfrag_conexiones.glsl");



		ptr = 0;
		store = false;

		dif = 0;


		lastMillis = 0;
		NperBeat =2;

		imgs = new PGraphics[NperBeat*6*16];
		timer = new TimedEventGenerator(pa);
		timer.setIntervalMs(750/NperBeat);
		timer.setEnabled(false);
          drawCam = false;
          version = 2;

	}

	/*
	void onTimerEvent() {
		  int millisDiff = millis() - lastMillis;
		  lastMillis = millisDiff + lastMillis;  
		  System.out.println("Got a timer event at " + millis() + "ms (" + millisDiff + ")!");
		  addImg(im);
//		  setRandomFillAndStroke();
//		  ellipse(random(width), random(height), random(100), random(100));
	}
	*/

	void setup(int w_, int h_, int cw_, int ch_){
		w = w_;
		h = h_;
		cw = cw_;
		ch = ch_;

		im = createImage(cw,ch,RGB);

		
		imag = createGraphics(cw,ch,P3D);

		rcw = cw/4;
		rch = ch/4;
		for(int i=0;i<imgs.length;i++){
			imgs[i] = createGraphics(rcw,rch,P3D);
		}
          negro = createGraphics(rcw,rch,P3D);
          negro.beginDraw();
          negro.background(0);
          negro.endDraw();

		imag.beginDraw();
		imag.background(0);
		imag.ellipse(cw/2,ch/2,10,10);
		imag.endDraw();


	//	addImg(imag);

		mask = createGraphics(w,h,P3D);


	}
  void resetBuffer(){
      for(int i=0;i<imgs.length;i++){
      imgs[i] = createGraphics(rcw,rch,P3D);
      }  
  }
  
	void addImage(){
		store = true;
	}

	void addImg(PImage imagen){
//		if(ptr<imgs.length){
		imgs[ptr].beginDraw();
//		imgs[ptr].image(imagen,0,0);
		imgs[ptr].beginShape(QUADS);
		imgs[ptr].texture(imagen);
		imgs[ptr].vertex(0,0, 0,0);
		imgs[ptr].vertex(rcw,0, imagen.width,0);
		imgs[ptr].vertex(rcw,rch, imagen.width, imagen.height);
		imgs[ptr].vertex(0,rch, 	0,imagen.height);
		imgs[ptr].endShape();
		/*
		imgs[ptr].textSize(50);
		imgs[ptr].text(ptr,200,200);
		*/
		imgs[ptr].endDraw();
		ptr = (ptr+1)%imgs.length;
//		}
	}

	int ptrDesfasado(int c){
		return (imgs.length+ptr-1-c*6*NperBeat)%imgs.length;
	}

	void setFrame(PImage img){
//		im.set(0,0,img);
		im = img; // Con la cámara así

	}

	void update(){
		if(selected && playing){
			int period = 750/NperBeat; // 750 ms per beat, over the number of frames per beat
			dif = (millis() - millis_begin)%period;
			if(store){
			//if(dif<period/5){
                           if(drawCam){
				addImg(im);
                           }
                           else{
                            addImg(negro); 
                           }
				store = false;
			}
		}
	}

	void draw(PGraphics pg){
		if(selected && playing){


		pg.beginDraw();
		pg.background(0);
		pg.shader(shader);
		pg.noStroke();
		pg.image(im,0,0);
		pg.fill(0);
		pg.rect(0,0,w,h);

		int rw = w/4;
		int rh = h/4;
          if(version==2){
           pg.translate(0,rh*1.5); 
          }
          int verlimit = (version==2) ? 1 : 4;
		for(int j=0;j<verlimit;j++){
			for(int i=0;i<4;i++){
				pg.beginShape(QUADS);
				pg.texture(imgs[ptrDesfasado(i+j*4)]);
//				pg.texture(im);
				pg.vertex(rw*i,rh*j, 0,0);
				pg.vertex(rw*(1+i),rh*j, rcw,0);
				pg.vertex(rw*(1+i),rh*(j+1), rcw,rch);
				pg.vertex(rw*i,rh*(j+1), 0,rch);
				pg.endShape();
			}
		}
		pg.endDraw();
		}
          else{
            pg.beginDraw();
            pg.background(0);
            pg.endDraw();
          }
	}

	void receiveKey(int k){
		if(k=='1' || k=='8'){
			selected = !selected;
                   println("Conexiones : "+(selected?"Selected":"Not selected"));
		}
		if(selected){
			switch(k){
				case ' ':
					playing = !playing;
                                  println("Conexiones: "+(playing?"Playing":"Not playing"));
					if(playing){
						millis_begin = millis();
						timer.setEnabled(true);
					}
                                   else{
                                      timer.setEnabled(false); 
                                   }
					break;
				case 'p':
					store = true;
					break;
                          case 'c':
                                  drawCam = !drawCam;
                                  println("Drawcam: "+(drawCam?"Enabled":"Disabled"));
                                  break;
                          case 'i':
                                  version = 2;
                                  println("Version 2");
                                  break;
                           case 'I':
                                  version = 3;
                                  println("Version 3");
                                  break;
                            case 'r':
                                  resetBuffer();
                                  println("Buffer reset");
                                  break;
			}
		}

	}


	boolean isSelected(){
		return selected;
	}


}