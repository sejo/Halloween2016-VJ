class Corazon implements Scene{
	int w, h, cw, ch;

	boolean selected;

	boolean playing;

	boolean nuevo;

	int counter, framecounter, targetframecounter;

	PImage im;
	PGraphics im2;

	PGraphics bg;

	PShader shader;

	int dir;
	int estado;


	float angle, targetangle;
	float t;

	Corazon(){
		selected = false;
		playing = false;
		angle = 0;
		t = random(1000);

		dir = 0;
		estado = 0;
		counter = 0;
		framecounter = 0;
		targetframecounter = 50;

		shader = loadShader("texfrag_corazon.glsl");

		nuevo = false;
	}

	void setup(int w_, int h_, int cw_, int ch_){
		w = w_;
		h = h_;
		cw = cw_;
		ch = ch_;

		im = createImage(cw,ch,RGB);
		im2 = createGraphics(cw,ch,P3D);
		bg = createGraphics(cw,ch,P3D);



	}

	void setFrame(PImage img){
//		im.set(0,0,img);
		im = img; // Con la cámara así

		im2.beginDraw();
		im2.image(im,0,0);
		im2.endDraw();


	}

	void update(){
		if(selected && playing){
			switch(estado){
				case 0:
					incangle();
					if(angle>targetangle){
						estado = 1;
						framecounter = 0;
					}
				break;
				case 1:
					framecounter++;
					if(framecounter>=targetframecounter){
						estado = 2;
						setTargetAngle(TWO_PI);
					}
					break;
				case 2:
					incangle();
					if(angle>targetangle){
						estado = 3;
						framecounter = 0;
					}
				break;
				case 3: // Pausa
					framecounter++;
					if(framecounter>=targetframecounter){
						estado = 4;
						setTargetAngle(TWO_PI*9/8);
					}
					break;
				case 4:
					incangle();
					if(angle>targetangle){
						estado = 5;
						framecounter = 0;
					}
				break;
				case 5: // Pausa
					framecounter++;
					if(framecounter>=targetframecounter){
						estado = 6;
						setTargetAngle(TWO_PI*5/4);
					}
					break;
				case 6:
					incangle();
					if(angle>targetangle){
						estado = 7;
						framecounter = 0;
					}
				break;
				case 7: // Pausa
					framecounter++;
					if(framecounter>=targetframecounter){
						estado = 8;
						setTargetAngle(TWO_PI*3/2);
					}
					break;
				case 8:
					incangle();
					if(angle>targetangle){
						estado = 9;
						framecounter = 0;
					}
					break;
				case 9: // Pausa
					framecounter++;
					if(framecounter>=targetframecounter){
						estado = 0;
						setTargetAngle(TWO_PI);
					}
					break;

			}
		t += 0.01;
		}
          else{
           initFSM(); 
          }
	}

	void setTargetAngle(float a){
		targetangle = angle + a;
	}

	void initFSM(){
		estado = 0;
		targetangle = angle + TWO_PI;
		dir = 0;

	}

	void incangle(){
		float inc = PI/256;
		if(dir==1){
			inc *= -1;
		}
		angle += inc;
	}

	void draw(PGraphics pg){

		if(selected && playing){
/*
		if(nuevo){
			bg.beginDraw();
			bg.image(im,0,0);
			bg.endDraw();
			nuevo = false;
		}*/
		float range = map(noise(t/10+10),0,1,PI/4,PI/2);
		int slices = 32;
		float ri = map(noise(t),0,1,10,20);
		float rf = h*0.8*map(noise(t/10+100),0,1,0.5,1.5);
		float a=angle;
		float ainc = range/slices;
		float c, s, c2, s2;
		float texfrac = 1.0*ch/slices;
		float texfrach = 1.0*cw/slices;

		float cx = w/2;
		float cy = h/2;


		shader.set("prevtext",im2);
		shader.set("background",bg);

		pg.beginDraw();
	//	pg.background(0);
		//pg.tint(255,100);
		pg.noStroke();
		pg.translate(cx,cy,0);
		pg.shader(shader);
		pg.beginShape(QUADS);
		pg.texture(im);

		float rf2;
		for(int i=0; i<slices; i++){
			c = cos(a);
			s = sin(a);
			c2 = cos(a+ainc);
			s2 = sin(a+ainc);
			/*
			pg.vertex(ri*c,ri*s, 0, i*texfrac);
			pg.vertex(rf*c,rf*s, cw,i*texfrac);
			pg.vertex(rf*c2,rf*s2, cw,(i+1)*texfrac);
			pg.vertex(ri*c2,ri*s2, 0,(i+1)*texfrac);
			*/
			pg.vertex(ri*c,ri*s, i*texfrach, ch);
			pg.vertex(rf*c,rf*s, i*texfrach,0);
			pg.vertex(rf*c2,rf*s2, (i+1)*texfrach,0);
			pg.vertex(ri*c2,ri*s2, (i+1)*texfrach,ch);
		
			a+=ainc;
		}

		pg.endShape();
		pg.endDraw();

		im2.beginDraw();
		im2.image(im,0,0);
		im2.endDraw();

		}
          else{
            pg.beginDraw();
            pg.background(0);
            pg.endDraw();
          }
	}

	void receiveKey(int k){
		if(k=='6'){
			selected = !selected;
		}
		if(selected){
			switch(k){
				case ' ':
					playing = !playing;
					if(playing){
						nuevo = true;
						initFSM();

					}
					break;
				case 'b':
					bg.beginDraw();
					bg.image(im,0,0);
					bg.endDraw();
					break;

			}
		}

	}


	boolean isSelected(){
		return selected;
	}


}