class MCM implements Scene{
	int w, h, cw, ch;

	boolean selected;

	boolean playing;

	PImage im;

	PGraphics mask;
	PShader shader;

	int counter, framecounter;
	int frameperiod;


	boolean[] mat;
	int[] ind;
	int N,M;

	MCM(){
		selected = false;
		counter = 0;

		int [] freqs = {4,5,6,7,12,20,28,30,35,42,60,84,140,210,420};
		N = 20;
		M = 21;

		frameperiod = 39; //92 bpm at 60 fps
          frameperiod = 13; // 92 bpm at 20 fps
          frameperiod = 7;

		mat = new boolean[N*M];
		ind = new int[N*M];

		for(int i=0;i<ind.length;i++){
			ind[i] = freqs.length-1;
			for(int j=0;j<4 && ind[i]>4;j++){
				if(i%freqs[j]==0){
					ind[i] = j;
				}
			}
			//println(i+" "+ind[i]);
			
			/*
			if(random(1)>0.7){
				ind[i] = int(random(freqs.length));
			}
			else{
				ind[i] = int(random(4));
			}
			*/
		}

		shader = loadShader("texfrag_mcm.glsl");
	}

	void setup(int w_, int h_, int cw_, int ch_){
		w = w_;
		h = h_;
		cw = cw_;
		ch = ch_;

		im = createImage(cw,ch,RGB);

		mask = createGraphics(w,h,P3D);


	}

	void setFrame(PImage img){
//		im.set(0,0,img);
		im = img; // Con la cámara así

	}

	void update(){
		int [] freqs = {4,5,6,7,12,20,28,30,35,42,60,84,140,210,420};
		if(selected && playing){
			if(framecounter%frameperiod == 0){
				for(int i=0;i<ind.length;i++){
					mat[i] = (counter%freqs[ind[i]] == 0);
				}
				counter++;
			}

			framecounter++;
		}
	}

	void draw(PGraphics pg){
		if(selected){
		mask.beginDraw();
		mask.clear();
		mask.background(0);
		mask.noStroke();
		int x,y;
		int wi = w/M;
		int he = h/N;
		for(int i=0;i<mat.length;i++){
			if(mat[i]){
				x=i/N;
				y=N-1-i%N;
				mask.rect(x*wi,y*he,wi,he);
			}
		}
		mask.endDraw();



		shader.set("mask",mask);


		pg.beginDraw();
		pg.shader(shader);
		pg.noStroke();
		pg.beginShape(QUADS);
		pg.texture(im);
		pg.vertex(0,0, 0,0);
		pg.vertex(w,0, cw,0);
		pg.vertex(w,h, cw,ch);
		pg.vertex(0,h, 0,ch);
		pg.endShape();
		pg.endDraw();
		}
          else{
            pg.beginDraw();
            pg.background(0);
            pg.endDraw();
          }
	}

	void receiveKey(int k){
		if(k=='7'){
			selected = !selected;
		}
		if(selected){
			switch(k){
				case ' ':
					playing = !playing;
					break;

			}
		}

	}


	boolean isSelected(){
		return selected;
	}


}