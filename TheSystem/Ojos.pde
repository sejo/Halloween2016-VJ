class Ojos implements Scene{
	int w, h, cw, ch;

	boolean selected;

	boolean playing;

	PImage im;

	PGraphics mask;
	PShader shader;

	int slices;
	float [] radios;
	float t;



	Ojos(){
		selected = false;
		playing = false;

		slices = 30;
		radios = new float[slices*slices];

		shader = loadShader("texfrag_ojos.glsl");

		t = 0;



	}

	void setup(int w_, int h_, int cw_, int ch_){
		w = w_;
		h = h_;
		cw = cw_;
		ch = ch_;

		im = createImage(cw,ch,RGB);

		mask = createGraphics(cw/4,ch/4,P3D);

		for(int i=0;i<radios.length;i++){
			radios[i] = w/4;
		}

		mask.beginDraw();
		mask.loadPixels();
		float x,y;
		for(int i=0;i<mask.pixels.length;i++){
			x = (i%mask.width)*0.01;
			y = (i/mask.width)*0.01;
			mask.pixels[i] = color(255*noise(x+500,y+500,t/20),255*noise(x,y,t/20),0);
		}
		mask.updatePixels();
		mask.endDraw();

		shader.set("mask",mask);


	}

	void setFrame(PImage img){
//		im.set(0,0,img);
		im = img; // Con la cámara así

	}

	void update(){
		if(selected && playing){
			float inc = 100;
			for(int i=0;i<radios.length;i++){
				radios[i] = map(noise(0.05*(i%slices),0.05*(i/slices),t),0,1,w*0.22-inc,w*0.22+inc);
			}
			
			mask.beginDraw();
			mask.loadPixels();
			float x,y;
			for(int i=0;i<mask.pixels.length;i++){
				x = (i%mask.width)*0.01;
				y = (i/mask.width)*0.01;
				mask.pixels[i] = color(255*noise(x+500,y+500,t),255*noise(x,y,t),0);
			}
			mask.updatePixels();
			mask.endDraw();

			shader.set("mask",mask);

			shader.set("range",map(noise(t),0,1,0,3500));

			
			t+=0.01;
		}
	}

	void draw(PGraphics pg){
		if(selected&&playing){

		float amargin = PI/6;
		float va, ha; // Vertical angle, horizontal angle
		float x,y,z,r;
		float ainc = (PI-2*amargin)/slices;
		float texfrach = 1.0*cw/(slices-1);
		float texfracv = 1.0*ch/(slices-1);


		pg.beginDraw();
		pg.background(0);
		pg.shader(shader);
		pg.noStroke();
		pg.fill(0,10);
		pg.rect(0,0,w,h);
		pg.stroke(255,0);
		pg.noFill();
		pg.translate(w/2,h/2,0);

		va = PI-amargin;
		pg.beginShape(QUADS);
		pg.texture(im);
		for(int i=0; i<slices-1;i++){
			ha = amargin;

		//	pg.stroke(i*255.0/slices,100);
			for(int j=0; j<slices-1; j++){
				r = radios[j + i*slices];
				x = r*cos(ha)*sin(va);
				y = r*cos(va);
				z = r*sin(ha)*sin(va);
				pg.vertex(x,y,z, j*texfrach,i*texfracv);

				r = radios[(j+1) + i*slices];
				x = r*cos(ha+ainc)*sin(va);
				y = r*cos(va);
				z = r*sin(ha+ainc)*sin(va);
				pg.vertex(x,y,z, (j+1)*texfrach,i*texfracv);

				r = radios[(j+1) + (i+1)*slices];
				x = r*cos(ha+ainc)*sin(va-ainc);
				y = r*cos(va-ainc);
				z = r*sin(ha+ainc)*sin(va-ainc);
				pg.vertex(x,y,z, (j+1)*texfrach,(i+1)*texfracv);

				r = radios[j + (i+1)*slices];
				x = r*cos(ha)*sin(va-ainc);
				y = r*cos(va-ainc);
				z = r*sin(ha)*sin(va-ainc);
				pg.vertex(x,y,z, j*texfrach,(i+1)*texfracv);
				ha += ainc;
			}
			va -= ainc;
		}
		pg.endShape();
//		image(mask,0,0);
		pg.endDraw();
		}
          else{
            pg.beginDraw();
            pg.background(0);
            pg.endDraw();
          }
	}

	void receiveKey(int k){
		if(k=='4'){
			selected = !selected;
		}
		if(selected){
			switch(k){
				case ' ':
					playing = !playing;
					break;

			}
		}

	}


	boolean isSelected(){
		return selected;
	}


}