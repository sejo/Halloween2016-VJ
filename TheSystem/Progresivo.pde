class Progresivo implements Scene{
	int w, h, cw, ch;

	boolean selected;

	boolean playing;

	PImage im;

	PGraphics mask;

	int [] freqs;
	int fac;

	PShader shader;

	float t;

	int counter,counterperiod;


	Progresivo(){
		selected = false;
		playing = false;


		shader = loadShader("texfrag_progresivo.glsl");
		

		t = 0;

		counter =0 ;
		counterperiod = 2;



	}

	void setup(int w_, int h_, int cw_, int ch_){
		w = w_;
		h = h_;
		cw = cw_;
		ch = ch_;

		im = createImage(cw,ch,RGB);

//		mask = createGraphics(1,ch,P3D); // Horizontal
		mask = createGraphics(cw,1,P3D); // Vertical


		fac = 30;
//		freqs = new int[mask.height/fac];  // Horizontal
		freqs = new int[mask.width/fac+1]; // Vertical

		mask.beginDraw();
		mask.loadPixels();
		float x,y;
		for(int i=0;i<mask.pixels.length;i++){
//			mask.pixels[i] = color(255*noise(i*0.1,t/20),255*noise(i*10+t/20),0);
			mask.pixels[i] = color(0);
			if(i%fac==0){
//			freqs[i/fac] = int(random(2,22))*5;
			freqs[i/fac] = int(random(8,50))*5;
			}
		}
		mask.updatePixels();
		mask.endDraw();



		shader.set("mask",mask);

	//	shader.set("range",cw);


	}

	void setFrame(PImage img){
//		im.set(0,0,img);
		im = img; // Con la cámara así

	}

	void update(){
		if(selected && playing){

			//if(counter%counterperiod==0){
			mask.beginDraw();
			mask.loadPixels();
			float x,y;
			color c = color(0);
			int r = 0;
			int g = 0;
			int b = 0;
			int val = 0;
			for(int i=0;i<mask.pixels.length;i++){
				if(i%fac==0){
				c = mask.pixels[i];
				val = int(red(c))*256 + int(green(c));
				val += freqs[i/fac];
				if(val>65535){
					val -= 65535;
				}
				r = (val>>8)&0xFF;
				g = (val)&0xFF;
				b = int(255*noise(i*0.1,t));
				/*
				r = red(c);
				r += freqs[i/fac];

				if(r>255){
					r -=255;	
				}

				*/
				}

				mask.pixels[i] = color(r,g,b);
			}
			mask.updatePixels();
			mask.endDraw();

			shader.set("mask",mask);
			//}

			
			t+=0.01;
			counter++;
		}
	}

	void draw(PGraphics pg){
		if(selected){

		pg.beginDraw();
//		pg.background(0);
		pg.noStroke();
		pg.fill(0,10);
		pg.rect(0,0,w,h);
		pg.stroke(255,0);
		pg.noFill();
		pg.shader(shader);
		pg.beginShape(QUADS);
		pg.texture(im);
		pg.vertex(0,0, 0,0);
		pg.vertex(w,0, cw,0);
		pg.vertex(w,h, cw,ch);
		pg.vertex(0,h, 0,ch);
	//	pg.image(mask,w/2,0,1,h);
		pg.endShape();
		pg.endDraw();
		}
          else{
            pg.beginDraw();
            pg.background(0);
            pg.endDraw();
          }
	}

	void receiveKey(int k){
		if(k=='2'){
			selected = !selected;
		}
		if(selected){
			switch(k){
				case ' ':
					playing = !playing;
					break;

			}
		}

	}


	boolean isSelected(){
		return selected;
	}


}
