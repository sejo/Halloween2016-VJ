class RandomWalkers implements Scene{
	int w, h, cw, ch;

	boolean selected;

	boolean playing;

	boolean store;

	PImage im;

	PGraphics mask;
	PShader shader;

	int counter, framecounter;
	int frameperiod;
	

	PGraphics [] imgs;
	int size;

	PGraphics imag;

	int N,M;

	RandomWalkers(){
		selected = false;
		counter = 0;

		N = 20;

		frameperiod =10; //92 bpm at 60 fps
    frameperiod =5;//
		shader = loadShader("texfrag_random.glsl");


		imgs = new PGraphics[120];
		size = 0;

	}

	void setup(int w_, int h_, int cw_, int ch_){
		w = w_;
		h = h_;
		cw = cw_;
		ch = ch_;

		im = createImage(cw,ch,RGB);

		
		imag = createGraphics(cw,ch,P3D);

		for(int i=0;i<imgs.length;i++){
			imgs[i] = createGraphics(cw,ch,P3D);
		}


		imag.beginDraw();
		imag.background(0);
		imag.ellipse(cw/2,ch/2,10,10);
		imag.endDraw();

	//	addImg(imag);

		mask = createGraphics(w,h,P3D);


		reloadMask();
	}

	void reloadMask(){
		mask.beginDraw();
		mask.background(0);
		mask.noStroke();
		mask.fill(255);
		float side;
		for(int i=0;i<100;i++){
		side = random(h/4);
		mask.rect(random(w*0.9),random(h*0.8),side,side);
		}
		mask.endDraw();

		shader.set("mask",mask);

	}

	void addImg(PImage imagen){
		if(size<imgs.length){
		imgs[size].beginDraw();
		imgs[size].image(imagen,0,0);
	/*	imgs[size].textSize(50);
		imgs[size].text(size,200,200);*/
		imgs[size].endDraw();
		size++;
		}
	}

	void setFrame(PImage img){
//		im.set(0,0,img);
		im = img; // Con la cámara así

	}

	void update(){
		if(selected && playing){
			if(framecounter%frameperiod == 0){

				if(size>0){
				counter = (size+counter-1)%size;

				if(counter==0){
					reloadMask();
				}
				}
			}


			if(store){
				addImg(im);
				store = false;
			}

			framecounter++;
		}
	}

	void draw(PGraphics pg){
		if(selected){

		pg.beginDraw();
		pg.background(0);
		pg.shader(shader);
		pg.noStroke();
		pg.image(im,0,0);
		pg.beginShape(QUADS);
		pg.texture(imgs[counter]);
		pg.vertex(0,0, 0,0);
		pg.vertex(w,0, cw,0);
		pg.vertex(w,h, cw,ch);
		pg.vertex(0,h, 0,ch);
		pg.endShape();
		/*
		pg.fill(255,0,0);
		pg.textSize(20);
		pg.text("Counter: "+counter, 10,50);
		pg.text("Size: "+imgs.length, 10,80);*/
		pg.endDraw();
		}
          else{
            pg.beginDraw();
            pg.background(0);
            pg.endDraw();
          }
	}

	void receiveKey(int k){
		if(k=='3'){
			selected = !selected;
		}
		if(selected){
			switch(k){
				case ' ':
					playing = !playing;
					break;
				case 'p':
					store = true;
					break;
				case 'r': // Reset
					size = 0;
					break;
				case 'j':
					if(frameperiod>2){
						frameperiod--;
					}
					println("Frameperiod: "+frameperiod);
					break;
				case 'k':
					frameperiod++;
					println("Frameperiod: "+frameperiod);
					break;


			}
		}

	}


	boolean isSelected(){
		return selected;
	}


}
