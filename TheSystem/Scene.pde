public interface Scene{
	void setup(int w_, int h_, int cw_, int ch_); // Screen w,h Camera w,h

	void setFrame(PImage img); // Update frame
	void update(); // Update accordingly
	void draw(PGraphics pg); // Draw in PGraphics

	void receiveKey(int k);

	boolean isSelected();
}
