import org.multiply.processing.*;
import processing.video.*;

/* CAM *
import processing.video.*;
Capture cam;
/* */

/* KINECT */
import KinectPV2.*;
KinectPV2 kinect;
/* */

Scene mcm;
Scene corazon;

Scene [] escenas;

Quad q;

PGraphics pg;

int selection;

boolean record;
boolean showText;

int lastMillis;

int x, y, w, h;

int framecounter;

Movie movie;
boolean enableMovie;
PImage splash;
boolean enableSplash;

void setup(){
//	size(800,600,P3D);
//	size(1280,720,P3D);
	fullScreen(P3D);

lastMillis = 0;

frameRate(60);

    pg = createGraphics(width,height,P3D);

    pg.beginDraw();
    pg.background(0);
    pg.endDraw();


	escenas = new Scene[7];
	escenas[0] = new Conexiones(this);
	escenas[1] = new Progresivo();
	escenas[2] = new RandomWalkers();
	escenas[3] = new Ojos();
	escenas[4] = new Carrerita();
	escenas[5] = new Corazon();
	escenas[6] = new MCM();


/* CAM *
   // cam = new Capture(this, 1280, 720, "/dev/video0", 30);
    cam = new Capture(this,1280,720);
    cam.start();
	for(int i=0;i<escenas.length;i++){
		escenas[i].setup(width,height,cam.width,cam.height);
	}
/* */

/* KINECT */
    kinect = new KinectPV2(this);
    kinect.enableColorImg(true);
    kinect.init();
	for(int i=0;i<escenas.length;i++){
  		escenas[i].setup(width,height,kinect.WIDTHColor,kinect.HEIGHTColor);
	}
/* */



	selection=0;

	record = false;
  showText = true;
  x =  200;
  y = 100;
  w = width -400;
  h = height -250;
  
  q = new Quad(x,y,w,h);

  framecounter = 0;
  
  
    /*
    movie = new Movie(this, "DESFASES.mov");
    enableMovie = false;
    
    splash = loadImage("splash.tif");
    enableSplash = false;
    */
  
}

void draw(){
//	background(0,200,0);
background(0);

/* CAM *
	if(cam.available()){
		cam.read();
	}
	if(escenas[selection].isSelected()){
		escenas[selection].setFrame(cam);
		escenas[selection].update();
		escenas[selection].draw(pg);
	}
	
/* */

/* KINECT */
	if(escenas[selection].isSelected()){
          	escenas[selection].setFrame(kinect.getColorImage());
		escenas[selection].update();
		escenas[selection].draw(pg);
	}
/* */




	image(pg,0,0,width,height);




  
  /*
  if(enableMovie){
    if (movie.available() == true) {
      movie.read(); 
    }
  
  blendMode(ADD);
  image(movie,0,0,width,height);
  blendMode(BLEND);
  }
  
  if(enableSplash){
    blendMode(SCREEN);
   image(splash,0,0,width,height); 
   blendMode(BLEND);
  }
  */

//  image(pg,x,y,w,h);
  /*
  q.drag(mousePressed,mouseX,mouseY);
  q.setTexture(pg);
  q.draw();*/

	if(record && frameCount%2==0){
		pg.save(String.format("screen-%05d.tif",framecounter));
		framecounter++;
	}

  if(showText){
	fill(255,0,0);
	textSize(20);
	text(frameRate,10,30);
  }

}

void keyPressed(){
	if(key>='1' && key<='8'){
		selection = key-'0';
		switch(selection){
			case 1: // Conexiones
				selection = 0;
				break;
			case 2: // Progresivo
				selection = 1;
				break;
			case 3: // Random walkers
				selection = 2;
				break;
			case 4: // Ojos
				selection = 3;
				break;
			case 5: // Carrerita
				selection = 4;
				break;
			case 6: // Corazon
				selection = 5;
				break;
			case 7: // MCM
				selection = 6;
				break;
			case 8: // Conexiones
				selection = 0;
				break;

		}
		pg.beginDraw();
		pg.clear();
		pg.background(0);
		pg.endDraw();
		for(int i=0;i<escenas.length;i++){
			escenas[i].receiveKey(key);
		}
	}
	else{
		escenas[selection].receiveKey(key);
		if(key=='S'){
			record = !record;
      print("Recording: "+(record?"Enabled":"Disabled"));
		}
		if(key=='s'){
			saveFrame();
		}
          if(key=='q'){
             showText = !showText; 
          }
          if(key=='Q'){
           enableSplash = !enableSplash;
          }
          switch(key){
             case 'J':
               q.rotateActive();
               break;
             case 'K':
               q.toggleShowVertex();
               break;
               
                 case 'A':
      q.decX();
    break;
    case 'X':
      q.incY();
    break;
    case 'D':
      q.incX();
    break;
    case 'W':
      q.decY();
    break;
    
    /*
    case '|':
      enableMovie = !enableMovie;
      print("Movie: "+(enableMovie?"Enabled":"Disabled"));
      break;
    case '!':
    movie = new Movie(this, "DESFASES.mov");
      movie.play();
      break;
        case '"':
    movie = new Movie(this, "Créditos.mov");
      movie.play();
      break;
            
	  */
          }

	}



}


  void onTimerEvent() {
    /*  int millisDiff = millis() - lastMillis;
      lastMillis = millisDiff + lastMillis;  
      System.out.println("Got a timer event at " + millis() + "ms (" + millisDiff + ")!");*/
      ((Conexiones)escenas[0]).addImage();
//      setRandomFillAndStroke();
//      ellipse(random(width), random(height), random(100), random(100));
  }
