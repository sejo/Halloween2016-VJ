#define PROCESSING_TEXTURE_SHADER

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;
uniform vec2 texOffset;

uniform sampler2D mask;


varying vec4 vertColor;
varying vec4 vertTexCoord;

void main() {

//	float d = texture2D(mask,vec2(vertTexCoord.s,1)).r; // Horizontal
	float d = texture2D(mask,vec2(1,vertTexCoord.t)).r; // Vertical

	if(d>0.1){
		vec4 color = texture2D(texture,vec2(1-vertTexCoord.s,vertTexCoord.t));
		gl_FragColor = vec4(color.rgb,1.0)*vertColor;
//		gl_FragColor = vec4(1);
	}
	else{
		gl_FragColor = vec4(0,0,0,0);
	}

}
