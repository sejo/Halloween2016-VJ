#define PROCESSING_TEXTURE_SHADER

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;
uniform vec2 texOffset;

uniform sampler2D prevtext;
uniform sampler2D background;


varying vec4 vertColor;
varying vec4 vertTexCoord;

void main() {


//	vec4 colorig = texture2D(texture,vertTexCoord.st);
	vec4 colorig = texture2D(texture,vec2(1-vertTexCoord.s,vertTexCoord.t));

//	vec4 col = texture2D(texture,vertTexCoord.st)-texture2D(prevtext,vertTexCoord.st);

	vec4 col = abs(texture2D(texture,vertTexCoord.st)-texture2D(background,vec2(vertTexCoord.s,1.0-vertTexCoord.t)));

	float avg = (col.r+col.g+col.b)/3.0;

	if(avg<0.2){
		avg = 0;
	}

	gl_FragColor = vec4(colorig.rgb,avg); // El alpha es el promedio de la diferencia entre background y frame actual


	/*
	gl_FragColor = vec4(colorig.rgb,1.0);

	gl_FragColor = vec4(col.rgb,1.0);

	*/

	//gl_FragColor = texture2D(background,vertTexCoord.st);



}
