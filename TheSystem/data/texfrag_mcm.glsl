#define PROCESSING_TEXTURE_SHADER

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;
uniform vec2 texOffset;

uniform sampler2D mask;


varying vec4 vertColor;
varying vec4 vertTexCoord;

void main() {

	float d = texture2D(mask,vertTexCoord.st).r;

	if(d>0.8){
		gl_FragColor = texture2D(texture,vec2(1-vertTexCoord.s,vertTexCoord.t))*vertColor;
	}
	else{
		gl_FragColor = vec4(0,0,0,0.06);
	}

}
