#define PROCESSING_TEXTURE_SHADER

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;
uniform vec2 texOffset;

uniform sampler2D mask;
uniform float range;


varying vec4 vertColor;
varying vec4 vertTexCoord;

void main() {

	vec4 offset = texture2D(mask,vertTexCoord.st);

	float x = offset.r-0.5;
	float y = offset.g-0.5;

	vec2 next = vertTexCoord.st + vec2((x*range)*texOffset.s,(y*range)*texOffset.t);

	int aux;
	if(next.s>1.0){
		aux = int(next.s);
		next = next + vec2(-float(aux), 0);
	}
	else if(next.s<0){
		aux = int(-next.s);
		next = next + vec2(float(aux+1),0);
	}
	


	vec4 color = texture2D(texture,next);
	gl_FragColor = vec4(color.rgb,1.0)*vertColor;



}
