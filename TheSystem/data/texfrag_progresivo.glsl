#define PROCESSING_TEXTURE_SHADER_

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;
uniform vec2 texOffset;

uniform sampler2D mask;
uniform float range;


varying vec4 vertColor;
varying vec4 vertTexCoord;

void main() {

//	vec4 desfase = texture2D(mask,vec2(1,vertTexCoord.t)); // Horizontal
	vec4 desfase = texture2D(mask,vec2(vertTexCoord.s,1));

	float x = desfase.r;
	float y = desfase.g;

	float z = desfase.b*0.2;

	float off = (x*256*256 + y*256)/65536.0;

//	vec2 next = vertTexCoord.st + vec2(off,0); // Horizontal
	vec2 next = vertTexCoord.st + vec2(0,off); // Horizontal

	int aux;
	if(next.s>1.0){
		aux = int(next.s);
		next = next + vec2(-float(aux), 0);
	}
	else if(next.s<0){
		aux = int(-next.s);
		next = next + vec2(float(aux+1),0);
	}
	if(next.t>1.0){
		aux = int(next.t);
		next = next + vec2(0,-float(aux));
	}
	else if(next.t<0){
		aux = int(-next.t);
		next = next + vec2(0,float(aux+1));
	}

	next = vec2(1-next.s,next.t);	
	vec4 color;
	if(vertTexCoord.t<z || vertTexCoord.t>1-z){
		color = vec4(0);
	}
	else{
	color = texture2D(texture,next);
	}

//	color = texture2D(texture,next);

	gl_FragColor = vec4(color.rgb,1)*vertColor;



}
