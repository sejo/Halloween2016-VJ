/*
    Who is dance?
    Interactive digital installation based on skeleton detection with Kinect v2

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class KinectSkeletonDepth{
    ArrayList<PVector> ve;
    KJoint[] joints;

    int skeletonArraySize;

    ArrayList<PVector> colors;
    
    void setup(){
       ve = new ArrayList<PVector>();

       skeletonArraySize = 0;
       colors = new ArrayList<PVector>();
    }

    int getSkeletonArraySize(){
    	return skeletonArraySize;
    }
 
  
  void update(){
    ve.clear();
    
    colors.clear();
    
     //get the skeletons as an Arraylist of KSkeletons
  ArrayList<KSkeleton> skeletonArray =  kinect.getSkeletonDepthMap();

  skeletonArraySize = skeletonArray.size();

  //individual joints
  for (int i = 0; i < skeletonArray.size(); i++) {
    KSkeleton skeleton = (KSkeleton) skeletonArray.get(i);
    //if the skeleton is being tracked compute the skleton joints
    if (skeleton.isTracked()) {
       joints = skeleton.getJoints();



      color col  = skeleton.getIndexColor();
	colors.add(new PVector(red(col),green(col),blue(col)));
 //     fill(col);
 //     stroke(col);
             

    addJoint(KinectPV2.JointType_SpineMid);
    addJoint(KinectPV2.JointType_SpineShoulder);
    addJoint(KinectPV2.JointType_Neck);
    addJoint(KinectPV2.JointType_Head);

    
    // Right Arm    
       addJoint(KinectPV2.JointType_ShoulderRight);
      addJoint( KinectPV2.JointType_ElbowRight);
      addJoint( KinectPV2.JointType_WristRight);
       addJoint(KinectPV2.JointType_HandRight);
      addJoint( KinectPV2.JointType_HandTipRight);
      addJoint( KinectPV2.JointType_ThumbRight);
    
    // Left Arm
       addJoint(KinectPV2.JointType_ShoulderLeft);
      addJoint( KinectPV2.JointType_ElbowLeft);
       addJoint(KinectPV2.JointType_WristLeft);
      addJoint( KinectPV2.JointType_HandLeft);
       addJoint(KinectPV2.JointType_HandTipLeft);
      addJoint( KinectPV2.JointType_ThumbLeft);

	//
   	addJoint(KinectPV2.JointType_SpineBase);
    
    // Right Leg
      addJoint( KinectPV2.JointType_HipRight);
       addJoint(KinectPV2.JointType_KneeRight);
      addJoint( KinectPV2.JointType_AnkleRight);
      addJoint( KinectPV2.JointType_FootRight);
    
    // Left Leg
       addJoint(KinectPV2.JointType_HipLeft);
       addJoint(KinectPV2.JointType_KneeLeft);
       addJoint(KinectPV2.JointType_AnkleLeft);
      addJoint( KinectPV2.JointType_FootLeft);
    
    }
  }
  }
 
   public ArrayList<PVector> getPoints(){
   return ve; 
  }

   public ArrayList<PVector> getColors(){
   return colors; 
  }
  
  void addJoint(int jointType){
   ve.add(new PVector(joints[jointType].getX(), joints[jointType].getY(), joints[jointType].getZ()));
  }
    
  
  
}
