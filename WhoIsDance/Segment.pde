/*
    Who is dance?
    Interactive digital installation based on skeleton detection with Kinect v2

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Segment{
	PVector origin, dest, dif, difunit, originXY,destXY,difXY,difunitXY;

	float dXYmag; // difXY mag
	float pmaxmag; // perpendicular maximum mag

	float range; // % of perpendicular range to the vector dif
	int mode;

	float jointSize;
	float strokeW;
	boolean showJoints;

	boolean enabled;

	float t; // "time"
	int it; // int time

	final int N_MODES = 10;


	Segment(){
		origin = new PVector();
		dest = new PVector();
		dif = new PVector();
		difunit = new PVector();

		originXY = new PVector();
		destXY = new PVector();
		difXY = new PVector();
		difunitXY = new PVector();

		enabled = true;

		showJoints = true;
		strokeW = 2.5;
		jointSize = 8;

		range = 0.35;

		mode = int(random(N_MODES));

		t = 0;
		it = 0;
	}

	// Get the projection of a 3D vector into the screen as a 2D vector
	private PVector sV(PVector v){
		return new PVector(sX(v),sY(v));
	}
	private float sX(PVector v){
		return screenX(v.x,v.y,v.z);
	}

	private float sY(PVector v){
		return screenY(v.x,v.y,v.z);
	}

	// Set the segment
	void setFromVectors(PVector a, PVector b){
		origin.set(a);
		originXY.set(sV(origin));
		dest.set(b);
		destXY.set(sV(dest));

		dif.set(dest);
		dif.sub(origin);
		difunit.set(dif);
		difunit.normalize();

		difXY.set(destXY);
		difXY.sub(originXY);

		dXYmag = difXY.mag();
		pmaxmag = dXYmag*range;

		difunitXY.set(difXY);
		difunitXY.normalize();
	}

	void setRange(float r){
		range = r;
		pmaxmag = dXYmag*range;
	}

	void draw(){
		if(enabled){
			noFill();
			stroke(255,255);
			strokeWeight(strokeW);
	
			// Segment
			pushMatrix();

			// Position in the origin with the corrresponding rotation towards dest
			translate(originXY.x,originXY.y,0);
			rotateZ(difXY.heading());
			float d;
			int nslices;
			switch(mode){
				case 0:
					line(0,0,dXYmag,0);
				break;
				case 1: // Bolitas avanzando
//					float distance = 15;
					float distance = dXYmag/5;
					int period = 20;
					// Displacement corresponding to moment in time
					float disp = (distance)*(it%period)/period;

					float dia = distance/4;
					// Now we can move only in the X axis
					for(d=disp;d<dXYmag;d+=distance){
						pushMatrix();
						translate(d,0);
						ellipse(0,0,5,5);
						popMatrix();
					}
				break;
				case 2: // Perlin noise
					beginShape();
					d=0;
					nslices = 20;
                                   vertex(0,0);
					for(int i=0;i<nslices;i++){
						vertex(d,map(noise(d,t),0,1,-pmaxmag/2,pmaxmag/2));	
						d+=dXYmag/nslices;
					}
                                   vertex(dXYmag,0);
					endShape();
				break;
				case 3: // Wreck (?)
					d=pmaxmag;
					arc(0,0,d,d, PI/2, 3*PI/2);
					line(0,-d/2,dXYmag,-d/2);
					line(0,d/2,dXYmag,d/2);
					arc(dXYmag,0,d,d, 3*PI/2, 5*PI/2);

				break;
				case 4: // sinoidal
					beginShape();
					d=0;
					nslices = 20;
					int nperiods =2;
                                   vertex(0,0);
					for(int i=0;i<nslices;i++){
						vertex(i*dXYmag/nslices,map(sin(d+t*10),-1,1,-pmaxmag/2,pmaxmag/2));	
						d+=nperiods*TWO_PI/nslices;
					}
                                  vertex(dXYmag,0);
					endShape();
				break;
				case 5: // Bezier
					bezier(0,0, dXYmag/2,pmaxmag/2, dXYmag/2,pmaxmag/2,   dXYmag,0);
					bezier(0,0, dXYmag/2,-pmaxmag/2, dXYmag/2,-pmaxmag/2,   dXYmag,0);
				break;
				case 6: // dashed line
					float l = 15;
					for(d=0;d<dXYmag;d+=l){
						line(d,0,d+l/2,0);
					}
				break;
				case 7: // Moving bezier
					int bezperiod = 30;
					float ybez;
					if( it%(bezperiod*2) < bezperiod){
					 ybez = map(it%bezperiod,0,bezperiod-1, -pmaxmag/2, pmaxmag/2);
					}
					else{
					 ybez = map(bezperiod-1-it%bezperiod,0,bezperiod-1, -pmaxmag/2, pmaxmag/2);
					}
					bezier(0,0, dXYmag/2,ybez, dXYmag/2,ybez,   dXYmag,0);
				break;
				case 8: // Triangles
					float trianglesize = 10;
					for(d=0;d<dXYmag-trianglesize;d+=trianglesize){
						beginShape();
						vertex(d,0,0);
						vertex(d+trianglesize,pmaxmag/2);
						vertex(d+trianglesize,-pmaxmag/2);
						endShape(CLOSE);
					}
				break;
				case 9: // Diamond
					beginShape();
					vertex(0,0);
					vertex(dXYmag/2,-pmaxmag/2);
					vertex(dXYmag,0);
					vertex(dXYmag/2,pmaxmag/2);
					endShape(CLOSE);
                                   line(dXYmag/2,-pmaxmag/2,dXYmag/2,pmaxmag/2);
				break;
			}

			// Joints
			if(showJoints){
				pushMatrix();
				ellipse(0,0,jointSize,jointSize);
				translate(dXYmag,0,0);
				ellipse(0,0,jointSize,jointSize);
				popMatrix();
			}	
			popMatrix();

		}

		updateMotion();
	}

	void updateMotion(){
		t += 0.01;
		it ++;
	}


	void enable(){
		enabled = true;
	}

	void disable(){
		enabled = false;
	}


}
