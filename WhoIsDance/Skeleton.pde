/*
    Who is dance?
    Interactive digital installation based on skeleton detection with Kinect v2

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class Skeleton{
	final int I_SPINE_MID = 0;
	final int I_SPINE_SHOULDER = 1;
	final int I_NECK = 2;
	final int I_HEAD = 3;
	final int I_SHOULDER_RIGHT = 4;
	final int I_ELBOW_RIGHT = 5;
	final int I_WRIST_RIGHT = 6;
	final int I_HAND_RIGHT = 7;
	final int I_HAND_TIP_RIGHT = 8;
	final int I_THUMB_RIGHT = 9;
	final int I_SHOULDER_LEFT = 10;
	final int I_ELBOW_LEFT = 11;
	final int I_WRIST_LEFT = 12;
	final int I_HAND_LEFT = 13;
	final int I_HAND_TIP_LEFT = 14;
	final int I_THUMB_LEFT = 15;
	final int I_SPINE_BASE = 16;
	final int I_HIP_RIGHT = 17;
	final int I_KNEE_RIGHT = 18;
	final int I_ANKLE_RIGHT = 19;
	final int I_FOOT_RIGHT = 20;
	final int I_HIP_LEFT = 21;
	final int I_KNEE_LEFT = 22;
	final int I_ANKLE_LEFT = 23;
	final int I_FOOT_LEFT = 24;

	final int I_LAST = I_FOOT_LEFT;

	final int I_SIZE = 25;

	final int I_FIRST_EXTRA = I_SIZE;

	final int I_EXTRA_SIZE = 6; // Extra segments
	final int I_SIZE_WITH_EXTRA = I_SIZE + I_EXTRA_SIZE;

	PVector [] ps, ps_buf; // points array, and points array buffer (for the non-transformed skeleton)
	float [] sfs; // scale factors

	int [] originps_i; // array of the indices of origin points for a given dest point index


	Segment [] segs; // Array of segments
	int torsoSegsMode; // Mode of drawing torso
	final int N_TORSO_MODES = 6;

	PVector col; // color of this skeleton

	int footrecordsize;
	float [] yfootleft, yfootright;
	float jumpsize;
	int handrecordsize;
	float [] yhandleft, yhandright;
	float wavesize;


	Skeleton(){
		// Initialize the points arrays
		ps = new PVector[I_SIZE];
		resetPoints();

		ps_buf = new PVector[I_SIZE];
		resetPointsBuffer();

		// Segments
		segs = new Segment[I_SIZE_WITH_EXTRA];

		// For every point there's an incoming vector that will be scaled, 
		// except for the first one (we give it a factor nevertheless)
		sfs = new float[I_SIZE]; 

		setupNewCharacter(); // Create segments, set random torso mode, and roulette of scale factors

		// List of the origin points corresponding to each destination point
		originps_i = new int[I_SIZE];
		setOriginPoints();

		//
		col = new PVector(255,255,255);

		footrecordsize = 5;
		handrecordsize = footrecordsize;
		yfootleft = new float[footrecordsize];
		yfootright = new float[footrecordsize];
		yhandleft = new float[handrecordsize];
		yhandright = new float[handrecordsize];
		
		for(int i=0;i<footrecordsize;i++){
			yfootleft[i] = 0;
			yfootright[i] = 0;
			yhandleft[i] = 0;
			yhandright[i] = 0;
		}
		jumpsize = 18;
		wavesize=18;

	}

	public void draw(){
		// Call the draw method of the segments
		for(int i=0;i<segs.length;i++){
			segs[i].draw();
		}
	}

	// Once the buffer has been set, call this method to get and update the scaled version
	public void updatePointsFromBufferWithScaling(){
		ps[I_SPINE_MID].set(ps_buf[I_SPINE_MID]);
		for(int i=0; i<I_SIZE; i++){ // We can skip the first one (0) because it's its own origin point
			updatePointFromBufferWithScaling(i);
		}
		updateSegments();
		
    // Setup a new character with an action (Work in progres...)
		//if(isJumping() || isWaving()){
    if(isJumping()){
			setupNewCharacter();
		}

	}

	// The list has the points of all the skeletons, so we have to select one with an index
	// The list has to be in the same order as the one used here
	public void setBufferFromListSelectSkeleton(ArrayList<PVector> list, int nskeleton){
		for(int i=0;i<I_SIZE;i++){
			ps_buf[i].set(list.get(i + nskeleton*I_SIZE));
		}
	}

	// The list should be in the same order as the one used here
	public void setBufferFromList(ArrayList<PVector> list){
		setBufferFromListSelectSkeleton(list,0);
	}

	public void setColor(PVector c){
		col.set(c);
	}

	// Given a color list, returns the index of the color that this has.
	// or -1 if the color is not present
	public int hasColorInList(ArrayList<PVector> colorlist){
		for(int i=0;i<colorlist.size();i++){
			if(hasColor(colorlist.get(i))){
				return i;
			}
		}
		
		return -1;
	}

	// Returns true if this skeleton has the given color
	public boolean hasColor(PVector c){
		return (int(c.x)==int(col.x)) && (int(c.y)==int(col.y)) && (int(c.z)==int(col.z));

	}

	private boolean isJumping(){
		for(int i=1;i<yfootleft.length;i++){
			yfootleft[i] = yfootleft[i-1];
			yfootright[i] = yfootright[i-1];
		}
		yfootleft[0] = ps[I_FOOT_LEFT].y;
		yfootright[0] = ps[I_FOOT_RIGHT].y;
		float d1 = yfootleft[footrecordsize-1]-yfootleft[0];
		float d2 = yfootright[footrecordsize-1]-yfootright[0];
		//println(String.format("%f %f",d1,d2));
		return (d1>jumpsize && d2>jumpsize);
	}

	private boolean isWaving(){
		for(int i=1;i<yhandleft.length;i++){
			yhandleft[i] = yhandleft[i-1];
			yhandright[i] = yhandright[i-1];
		}
		yhandleft[0] = ps[I_HAND_LEFT].y;
		yhandright[0] = ps[I_HAND_RIGHT].y;
		float d1 = yhandleft[handrecordsize-1]-yhandleft[0];
		float d2 = yhandright[handrecordsize-1]-yhandright[0];
		//println(String.format("%f %f",d1,d2));
		return (d1>wavesize && d2>wavesize);
	}
	

	private void setupNewCharacter(){
		createSegments();
		setRandomTorsoMode();
		rouletteScaleFactors();
	}

	private void updatePointFromBufferWithScaling(int i){

		 // Get the index of the "origin" point for this segment
		int originpointindex = originps_i[i];

		// Get the transformed vector relative to the positions in the buffer with the given scaling
		PVector dif = getTransformedVector(ps_buf[originpointindex], ps_buf[i], sfs[i]);
		
		// Set origin position from current skeleton (the one that should be in the process of being updated)
		PVector newp = new PVector();
		newp.set(ps[originpointindex]); 

		// Add the vector to the point to get the transformed point
		newp.add(dif);

		// Update the point
		ps[i].set(newp);

	}

	private PVector getTransformedVector(PVector origin, PVector dest, float scaleFactor){
		PVector dif = PVector.sub(dest,origin); // Gets the difference..
		dif.mult(scaleFactor); // ...That gets its magnitude scaled

		return dif; // Return the new destination vector
	}



	private void resetPoints(){
		for(int i=0;i<ps.length;i++){
			ps[i] = new PVector();
		}
	}

	private void resetPointsBuffer(){
		for(int i=0;i<ps_buf.length;i++){
			ps_buf[i] = new PVector();
		}
	}

	private void resetScaleFactors(){
		for(int i=0;i<I_SIZE;i++){
			sfs[i] = 1.0;
		}
	}
	

	private color getColAsColor(){
		return color(col.x,col.y,col.z);
	}

	private void drawPoint(PVector p){
		
		pushMatrix();
		translate(p.x,p.y,p.z);
		ellipse(0,0,5,5);
		popMatrix();
	}

	private void drawSegment(PVector a, PVector b){
		pushMatrix();
		line(a.x,a.y,a.z,  b.x, b.y, b.z);
		popMatrix();
	}

	private void setOriginPoints(){
		originps_i[I_SPINE_MID] = I_SPINE_MID;
		originps_i[I_SPINE_SHOULDER] = I_SPINE_MID;
		originps_i[I_NECK] = I_SPINE_SHOULDER;
		originps_i[I_HEAD] = I_NECK;
		originps_i[I_SHOULDER_RIGHT] = I_SPINE_SHOULDER;
		originps_i[I_ELBOW_RIGHT] = I_SHOULDER_RIGHT;
		originps_i[I_WRIST_RIGHT] = I_ELBOW_RIGHT;
		originps_i[I_HAND_RIGHT] = I_WRIST_RIGHT;
		originps_i[I_HAND_TIP_RIGHT] = I_HAND_RIGHT;
		originps_i[I_THUMB_RIGHT] = I_HAND_RIGHT;
		originps_i[I_SHOULDER_LEFT] = I_SPINE_SHOULDER;
		originps_i[I_ELBOW_LEFT] = I_SHOULDER_LEFT;
		originps_i[I_WRIST_LEFT] = I_ELBOW_LEFT;
		originps_i[I_HAND_LEFT] = I_WRIST_LEFT;
		originps_i[I_HAND_TIP_LEFT] = I_HAND_LEFT;
		originps_i[I_THUMB_LEFT] = I_HAND_LEFT;
		originps_i[I_SPINE_BASE] = I_SPINE_MID;
		originps_i[I_HIP_RIGHT] = I_SPINE_BASE;
		originps_i[I_KNEE_RIGHT] = I_HIP_RIGHT;
		originps_i[I_ANKLE_RIGHT] = I_KNEE_RIGHT;
		originps_i[I_FOOT_RIGHT] = I_ANKLE_RIGHT;
		originps_i[I_HIP_LEFT] = I_SPINE_BASE;
		originps_i[I_KNEE_LEFT] = I_HIP_LEFT;
		originps_i[I_ANKLE_LEFT] = I_KNEE_LEFT;
		originps_i[I_FOOT_LEFT] = I_ANKLE_LEFT;

	}


	private void setRandomTorsoMode(){
		setTorsoMode(int(random(N_TORSO_MODES)));
	}

	private void setTorsoMode(int m){
		torsoSegsMode = m;

		switch(torsoSegsMode){
			case 0:
				segs[I_SHOULDER_LEFT].disable();
				segs[I_SHOULDER_RIGHT].disable();
				segs[I_HIP_LEFT].disable();
				segs[I_HIP_RIGHT].disable();
				segs[I_SPINE_BASE].disable();
				segs[I_SPINE_SHOULDER].disable();		
				segs[I_FIRST_EXTRA].disable();
				segs[I_FIRST_EXTRA+1].disable();
				segs[I_FIRST_EXTRA+2].disable();
				segs[I_FIRST_EXTRA+3].disable();
				segs[I_FIRST_EXTRA+4].enable();
				segs[I_FIRST_EXTRA+5].enable();
			break;
			case 1:
				segs[I_SHOULDER_LEFT].disable();
				segs[I_SHOULDER_RIGHT].disable();
				segs[I_HIP_LEFT].disable();
				segs[I_HIP_RIGHT].disable();
				segs[I_SPINE_BASE].disable();
				segs[I_SPINE_SHOULDER].disable();		
				segs[I_FIRST_EXTRA].enable();
				segs[I_FIRST_EXTRA+1].enable();
				segs[I_FIRST_EXTRA+2].disable();
				segs[I_FIRST_EXTRA+3].disable();
				segs[I_FIRST_EXTRA+4].disable();
				segs[I_FIRST_EXTRA+5].disable();
			break;
			case 2:
				segs[I_SHOULDER_LEFT].disable();
				segs[I_SHOULDER_RIGHT].disable();
				segs[I_HIP_LEFT].disable();
				segs[I_HIP_RIGHT].disable();
				segs[I_SPINE_BASE].enable();
				segs[I_SPINE_SHOULDER].enable();		
				segs[I_FIRST_EXTRA].disable();
				segs[I_FIRST_EXTRA+1].disable();
				segs[I_FIRST_EXTRA+2].disable();
				segs[I_FIRST_EXTRA+3].disable();
				segs[I_FIRST_EXTRA+4].disable();
				segs[I_FIRST_EXTRA+5].disable();
			break;
			case 3:
				segs[I_SHOULDER_LEFT].disable();
				segs[I_SHOULDER_RIGHT].disable();
				segs[I_HIP_LEFT].disable();
				segs[I_HIP_RIGHT].disable();
				segs[I_SPINE_BASE].disable();
				segs[I_SPINE_SHOULDER].disable();		
				segs[I_FIRST_EXTRA].enable();
				segs[I_FIRST_EXTRA+1].enable();
				segs[I_FIRST_EXTRA+2].enable();
				segs[I_FIRST_EXTRA+3].enable();
				segs[I_FIRST_EXTRA+4].disable();
				segs[I_FIRST_EXTRA+5].disable();
			break;
			case 4:
				segs[I_SHOULDER_LEFT].disable();
				segs[I_SHOULDER_RIGHT].disable();
				segs[I_HIP_LEFT].disable();
				segs[I_HIP_RIGHT].disable();
				segs[I_SPINE_BASE].disable();
				segs[I_SPINE_SHOULDER].disable();		
				segs[I_FIRST_EXTRA].disable();
				segs[I_FIRST_EXTRA+1].disable();
				segs[I_FIRST_EXTRA+2].enable();
				segs[I_FIRST_EXTRA+3].enable();
				segs[I_FIRST_EXTRA+4].disable();
				segs[I_FIRST_EXTRA+5].disable();
			break;
			case 5:
				segs[I_SHOULDER_LEFT].disable();
				segs[I_SHOULDER_RIGHT].disable();
				segs[I_HIP_LEFT].disable();
				segs[I_HIP_RIGHT].disable();
				segs[I_SPINE_BASE].enable();
				segs[I_SPINE_SHOULDER].enable();		
				segs[I_FIRST_EXTRA].disable();
				segs[I_FIRST_EXTRA+1].disable();
				segs[I_FIRST_EXTRA+2].enable();
				segs[I_FIRST_EXTRA+3].enable();
				segs[I_FIRST_EXTRA+4].disable();
				segs[I_FIRST_EXTRA+5].disable();
			break;
		}
	}

	private void createSegments(){
		for(int i=0;i<segs.length;i++){
			segs[i] = new Segment();
			if(i>=I_SIZE){ // Extra segments
				segs[i].disable();
				segs[i].setRange(0.1);
			}
		}
		// Disable every segment of the torso
		segs[I_SHOULDER_LEFT].disable();
		segs[I_SHOULDER_RIGHT].disable();
		segs[I_HIP_LEFT].disable();
		segs[I_HIP_RIGHT].disable();
		segs[I_SPINE_BASE].disable();
		segs[I_SPINE_MID].disable();
		segs[I_SPINE_SHOULDER].disable();

		segs[I_HEAD].setRange(1.0);

	}

	private void updateSegments(){
		for(int i=0;i<segs.length;i++){
			if(i<I_SIZE){ // Normal segments
				segs[i].setFromVectors(ps[originps_i[i]],ps[i]);
			}
			else{ // Extra segments
				switch(i-I_SIZE){
					case 0:
						segs[i].setFromVectors(ps[I_SHOULDER_LEFT],ps[I_SHOULDER_RIGHT]);
					break;
					case 1:
						segs[i].setFromVectors(ps[I_HIP_LEFT],ps[I_HIP_RIGHT]);
					break;
					case 2:
						segs[i].setFromVectors(ps[I_SHOULDER_LEFT],ps[I_HIP_LEFT]);
					break;
					case 3:
						segs[i].setFromVectors(ps[I_SHOULDER_RIGHT],ps[I_HIP_RIGHT]);
					break;
					case 4:
						segs[i].setFromVectors(ps[I_SHOULDER_LEFT],ps[I_HIP_RIGHT]);
					break;
					case 5:
						segs[i].setFromVectors(ps[I_HIP_LEFT],ps[I_SHOULDER_RIGHT]);
					break;
				}
			}
		}

	}

	private void rouletteScaleFactors(){
		resetScaleFactors();
		int n = int(random(1,4));

		for(int i=0;i<n;i++){
			randomizeScaleFactors();
		}

	}


	private void randomizeScaleFactors(){
		float sc;
		if(random(1)<0.5){
			sc = random(0.2,0.4);
		}
		else{
			sc = random(2.0,4.0);
		}

		switch(int(random(85))){
			case 0:
			break;
			case 1:  // Cabeza
			 sfs[I_HEAD] = sc;
			break;
			case 2: // Cuello
			 sfs[I_NECK] = sc;
			break;
			case 3: // Hombros
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			break;
			case 4: // Brazos
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			break;
			case 5: // Antebrazos
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			break;
			case 6: // Manos
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			break;
			case 7: // Torso (columna)
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			break;
			case 8: // Caderas
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 9: // Muslos
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc;
			break;
			case 10: // Espinilla
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 11: // Pies
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 12: // Cabeza + cuello
			 sfs[I_HEAD] = sc;
			 sfs[I_NECK] = sc;
			break;
			case 13: // Cabeza + hombros
			 sfs[I_HEAD] = sc;
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			break;
			case 14: // Cabeza + brazos
			 sfs[I_HEAD] = sc;
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			break;
			case 15: // Cabeza + antebrazos
			 sfs[I_HEAD] = sc;
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			break;
			case 16: // Cabeza + manos
			 sfs[I_HEAD] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			break;
			case 17: // Cabeza + torso
			 sfs[I_HEAD] = sc;
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			break;
			case 18: // Cabeza + caderas
			 sfs[I_HEAD] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 19: // Cabeza + piernas
			 sfs[I_HEAD] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 20: // Cabeza + muslos
			 sfs[I_HEAD] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			break;
			case 21: // Cabeza + espinillas
			 sfs[I_HEAD] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 22: // Cabeza + pies
			 sfs[I_HEAD] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 23: // Cuello + hombros
			 sfs[I_NECK] = sc;
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc; 
			break;
			case 24: // Cuello + brazos
			 sfs[I_NECK] = sc;
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			break;
			case 25: // Cuello + antebrazos
			 sfs[I_NECK] = sc;
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			break;
			case 26: // Cuello + manos
			 sfs[I_NECK] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			break;
			case 27: // Cuello + torso
			 sfs[I_NECK] = sc;
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			break;
			case 28: // Cuello + caderas
			 sfs[I_NECK] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 29: // Cuello + piernas
			 sfs[I_NECK] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 30: // Cuello + muslos
			 sfs[I_NECK] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			break;
			case 31: // Cuello + espinillas
			 sfs[I_NECK] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 32: // Cuello + pies
			 sfs[I_NECK] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 33: // Hombros + brazos
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			break;
			case 34: // Hombros + antebrazos
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			break;
			case 35: // Hombros  + manos
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			break;
			case 36: // Hombros + torso
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			break;
			case 37: // Hombros + caderas
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 38: // Hombros  + piernas
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 39: // Hombros + muslos
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			break;
			case 40: // Hombros + espinillas
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 41: // Hombros + pies
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 42: // Brazos  + antebrazos
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			break;
			case 43: // Brazos + manos
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			break;
			case 44: // Brazos + torso
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			break;
			case 45: // Brazos  + caderas
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 46: // Brazos  + piernas
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 47: // Brazos  + muslos
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			break;
			case 48: // Brazos + espinillas
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 49: // Brazos + pies
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 50: // Antebrazos + manos
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			break;
			case 51: // Antebrazos + torso
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			break;
			case 52: // Antebrazos + caderas
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 53: // Antebrazos + piernas
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 54: // Antebrazos + muslos
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			break;
			case 55: // Antebrazos + espinillas
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 56: // Antebrazos + pies
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 57: // Manos  + torso
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			break;
			case 58: // Manos + caderas
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 59: // Manos + piernas
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 60: // Manos + muslos
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			break;
			case 61: // Manos + espinillas
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 62: // Manos + pies
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 63: // Torso  + caderas
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 64: // Torso + piernas
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 65: // Torso + muslos
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			break;
			case 66: // Torso + espinillas
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 67: // Torso + pies
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 68: // Caderas + piernas
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 69: // Caderas + muslos
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			break;
			case 70: // Caderas  + espinillas
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 71: // Caderas + pies
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 72: // Muslos + espinillas
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 73: // Muslos + pies
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 74: // Espinillas + pies
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 75: // Cabeza + caderas + pies
			 sfs[I_HEAD] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 76: // Cabeza + antebrazos + espinillas
			 sfs[I_HEAD] = sc;
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			break;
			case 77: // Cabeza + manos + pies
			 sfs[I_HEAD] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 78: // Cuello + extremidades
			 sfs[I_NECK] = sc;
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 79: // Cuello + manos + pies
			 sfs[I_NECK] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 80: // Extremidades
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 81: // Extremidades izquierdas
			 sfs[I_ELBOW_LEFT] = sc;
			 sfs[I_WRIST_LEFT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_KNEE_LEFT] = sc; 
			 sfs[I_ANKLE_LEFT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
			case 82: // Extremidades derechas
			 sfs[I_ELBOW_RIGHT] = sc;
			 sfs[I_WRIST_RIGHT] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_KNEE_RIGHT] = sc;
			 sfs[I_ANKLE_RIGHT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			break;
			case 83: // Hombros + torso + caderas
			 sfs[I_SHOULDER_RIGHT] = sc;
			 sfs[I_SHOULDER_LEFT] = sc;
			 sfs[I_SPINE_BASE] = sc;
			 sfs[I_SPINE_SHOULDER] = sc;
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			break;
			case 84: // Caderas + manos + pies
			 sfs[I_HIP_RIGHT] = sc;
			 sfs[I_HIP_LEFT] = sc;
			 sfs[I_HAND_RIGHT] = sc;
			 sfs[I_HAND_TIP_RIGHT] = sc;
			 sfs[I_THUMB_RIGHT] = sc;
			 sfs[I_HAND_LEFT] = sc;
			 sfs[I_HAND_TIP_LEFT] = sc;
			 sfs[I_THUMB_LEFT] = sc;
			 sfs[I_FOOT_RIGHT] = sc;
			 sfs[I_FOOT_LEFT] = sc;
			break;
		}

	}



}