/*
    Who is dance?
    Interactive digital installation based on skeleton detection with Kinect v2

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

class SkeletonManager{
	ArrayList<Skeleton> skes;

	ArrayList<PVector> points;
	ArrayList<PVector> colors;

	final int I_SIZE = 25;

	int nSkeletonsInPointsList;


	SkeletonManager(){
		skes = new ArrayList<Skeleton>();
		points = new ArrayList<PVector>();
		colors = new ArrayList<PVector>();

		nSkeletonsInPointsList = 0;

	}

	public void draw(){
		Skeleton sk;
		for(int i=0;i<skes.size();i++){
			sk = skes.get(i);
			sk.draw();
		}
	}
	

	public void updateSkeletonsWithLists(ArrayList<PVector> ps, ArrayList<PVector> cs){
		points.clear();
		points.addAll(ps);

		nSkeletonsInPointsList = points.size()/I_SIZE;

		colors.clear();
		colors.addAll(cs);

		updateSkeletons();
	}

	public int getNumberOfSkeletons(){
		return skes.size();
	}


	private void updateSkeletons(){
		Skeleton sk;
		int index;
		// Flags array
		boolean [] found = new boolean[nSkeletonsInPointsList];
		for(int i=0;i<found.length;i++){
			found[i] = false;
		}

		// Loop for updating the existing skeletons with corresponding colors
		for(int i=0;i<skes.size();i++){
			index = skes.get(i).hasColorInList(colors);
			if(index<0){ // skeleton doesn't have any of the current colors
				skes.remove(i);// We remove it
			}
			else{
				// Update the skeleton with the corresponding color
				found[index] = true; // Flag this color as taken
				skes.get(i).setBufferFromListSelectSkeleton(points, index);
				skes.get(i).updatePointsFromBufferWithScaling();
			}
		}

		// Check if there are missing skeletons that need to be created
		if(skes.size()<nSkeletonsInPointsList){
			for(int i=0;i<nSkeletonsInPointsList;i++){
				if(!found[i]){ // Only create the ones that are needed
					sk = new Skeleton();
					sk.setColor(colors.get(i));
					sk.setBufferFromListSelectSkeleton(points, i);
					sk.updatePointsFromBufferWithScaling();
					skes.add(sk);
				}
			}

		}
	}
}
