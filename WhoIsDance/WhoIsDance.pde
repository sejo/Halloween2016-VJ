/*
    Who is dance?
    Interactive digital installation based on skeleton detection with Kinect v2

    Copyright (C) 2016 José Vega-Cebrián (Sejo) 
    jmvc[at]escenaconsejo.org - www.escenaconsejo.org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import KinectPV2.*;

KinectPV2 kinect; // Kinect library object
KinectSkeletonDepth ksd; // Manager of the Kinect information

SkeletonManager skes; // Manager of the skeletons (points, indices, segment types...)

float ytranslate;
float scaleFactor;

PFont font;

String transformationsPath = "data/transforms.txt";

void setup(){
	fullScreen(P3D);

  // Load font
  font = loadFont("WebServeroff-48.vlw");
  textFont(font);


	// Skeleton Manager
	skes = new SkeletonManager();

	// Kinect
  println("Starting Kinect...");
	kinect = new KinectPV2(this);
	kinect.enableDepthImg(true);
	kinect.enableSkeletonDepthMap(true);
	kinect.init();
	ksd = new KinectSkeletonDepth();
	ksd.setup();



	// Try to load the Scaling and y translation
  // If the file doesn't exist, set and create it
  if(!loadTransformations()){
    resetTransformations();
    saveTransformations();
  }
  
  println("Setup complete!");

}

void draw(){
	background(0);


	pushMatrix();
  // Apply transformations for skeletons
	translate(0,ytranslate,0);
	scale(scaleFactor,scaleFactor);

	// Update data from kinect
	ksd.update();
	// Update the SkeletonManager with new data
	skes.updateSkeletonsWithLists(ksd.getPoints(),ksd.getColors());
	// Draw the Skeletons
	skes.draw();
	popMatrix();
  

  // Draw the credits
  drawCredits();
}





// Helper functions
void drawCredits(){
    // Title and credits
  String[] credits = new String[3];
  credits[0] = "Who is dance? (2016)";
  credits[1] = "Sejo Vega-Cebrián";
  credits[2] = "escenaconsejo.org/whoisdance";
  // Size of the credits text
  float creditsSize = height*0.03;
  // Right margin x, to align text there
  float rightmarginx = width*0.97;
  // Y to start drawing the credits
  float y = height*0.85;
  
  float x = 0;
  textSize(creditsSize);
  for(int i=0; i<credits.length; i++){
    // Align text
   x = rightmarginx - textWidth(credits[i]);  
   // Draw text
   text(credits[i],x,y);
   // Increment Y  
   y += creditsSize*1.2;
  }
 
}

void saveFrameWithDate(){
	save(String.format("frame-%04d%02d%02d%02d%02d%02d%03d.png",year(),month(),day(),hour(),minute(),second(),frameCount%1000));
}


void saveTransformations(){
   String [] data = {str(ytranslate), str(scaleFactor)}; 
   saveStrings(transformationsPath,data);
}

boolean loadTransformations(){
  String[] data = loadStrings(transformationsPath);
  if(data==null){ // If file doesn't exist, return false
   return false; 
  }
  ytranslate = float(data[0]);
  scaleFactor = float(data[1]);
  return true;
}

void resetTransformations(){ // Default transforms
  scaleFactor = width/kinect.WIDTHDepth;
  ytranslate = 0; 
}

void keyPressed(){
	switch(key){
		// Y translation
		case 'j':
			ytranslate += 10;
		break;
		case 'k':
			ytranslate -= 10;
		break;
		case 'J':
			ytranslate += 100;
		break;
		case 'K':
			ytranslate -= 100;
		break;

		// Scale factor
		case 'n':
			scaleFactor -= 0.1;
		break;
		case 'm':
			scaleFactor += 0.1;
		break;

    // Reset all transformations
    case 'r':
      resetTransformations();
      break;
	}
  println(String.format("Translate-y: %f, Scale: %f",ytranslate,scaleFactor));
  saveTransformations();

}